import { TestBed } from '@angular/core/testing';

import { ReportService } from './report.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './user.service';
import { Session } from '../models/session';

describe('ReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService]
  }));

  it('should be created', () => {
    const service: ReportService = TestBed.get(ReportService);
    expect(service).toBeTruthy();
  });

  it('should get all reports', () => {
    const service: ReportService = TestBed.get(ReportService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.getAllReports();
    expect(val === undefined).toBe(false);
  });

  it('should create a report', () => {
    const service: ReportService = TestBed.get(ReportService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.createReport('type', 'reason', 1337);
    expect(val === undefined).toBe(false);
  });

  it('should delete a report', () => {
    const service: ReportService = TestBed.get(ReportService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.deleteReport(42);
    expect(val === undefined).toBe(false);
  });
});
