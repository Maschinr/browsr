import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from '../models/post';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { Comment } from '../models/comment';
import { PostTypes } from "../components/enums/postTypeEnum";
import { Ratings } from "../components/enums/ratingEnum";
import { ListType } from "../components/enums/listTypeEnum";
import { Direction } from "../components/enums/directionEnum";
import { Criteria } from "../components/enums/criteriaEnum";


@Injectable({
    providedIn: 'root'
})

export class PostService {

    public Tags: string[] = [];
    post: Post;

    constructor(private http: HttpClient, private userService: UserService) {

    }

    public getAll(type: ListType, user: string = '', tagName: string = '', start: number = 0, direction: Direction = Direction.ASCENDING, limit: number = 10, tags: string[] = [], criteria: Criteria = Criteria.TIME): Observable<Post[]> {
        let query: string = '?start=' + start + '&direction=' + direction + '&limit=' + limit + '&criteria=' + criteria;

        tags.forEach(n => {
            query += '&tags=' + n;
        });

        if (type === ListType.HIGHLIGHT) {
            return this.http.get<Post[]>('https://localhost:3000/api/posts/highlight' + query);
        } else if (type === ListType.ARCHIVE) {
            return this.http.get<Post[]>('https://localhost:3000/api/posts/archive' + query);
        } else if (type === ListType.TAG) {
            if (tagName === '') {
                console.error('TagName not given!');
                return undefined;
            }
            query += '&name=' + tagName;
            return this.http.get<Post[]>('https://localhost:3000/api/posts/tag' + query);
        } else if (type === ListType.USER) {
            if (user === '') {
                console.error('User not given!');
                return undefined;
            }
            query += '&user=' + user;
            return this.http.get<Post[]>('https://localhost:3000/api/posts/user' + query);
        } else {
            console.error('Invalid type given!');
            return undefined;
        }
    }

    public deletePost(postid: number) {
        if (this.userService.isLoggedIn() === false) {
            return undefined;
        }

        const model = {
            postid,
            sessionkey: this.userService.session.sessionkey,
            user: this.userService.session.username
        };
        return this.http.post<any>('https://localhost:3000/api/posts/delete', model);
    }

    public createPost(post: Post) {
        if (this.userService.isLoggedIn() === false) {
            throw new Error('Not logged in');
        }

        if (post === undefined) {
            throw new Error('Invalid Post');
        }

        if (post.content === '' || post.title === undefined || post.ptag.name === '' || post.stag.name === '' || post.type === undefined) {
            throw new Error('Invalid Post');
        }

        const fd = new FormData();
        fd.set('content', post.content);
        fd.set('title', post.title);
        fd.set('user', this.userService.session.username);
        fd.set('sessionkey', this.userService.session.sessionkey);
        fd.set('ptag', post.ptag.name);
        fd.set('stag', post.stag.name);
        fd.set('type', post.type);

        const headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');

        return this.http.post('https://localhost:3000/api/posts/upload', fd, {
            headers
        });
    }

    public ratePost(postid: number, rating: Ratings) {
        if (this.userService.isLoggedIn() === false) {
            return undefined;
        }

        const model = {
            postid,
            rating,
            sessionkey: this.userService.session.sessionkey,
            user: this.userService.session.username
        };
        return this.http.post<any>('https://localhost:3000/api/posts/rate', model);
    }

    public writeComment(postid: number, text: string, parentid?: number) {
        const model = {
            sessionkey: this.userService.session.sessionkey,
            postid,
            text,
            parentid
        };
        return this.http.post('https://localhost:3000/api/comment/postcomment', model);
    }

    public deleteComment(id: number) {
        const model = {
          sessionkey: this.userService.session.sessionkey,
          id
        };
        return this.http.post('https://localhost:3000/api/comment/deletecomment', model);
    }

    public getPostById(postid: number) {
        //https://localhost:3000/api/posts/
        return this.http.get<Post>('https://localhost:3000/api/posts/single?postid=' + postid);
    }

    public getComments(postid: number) {

        return this.http.get<Comment[]>('https://localhost:3000/api/comment/getcomments?postid=' + postid);
    }
}
