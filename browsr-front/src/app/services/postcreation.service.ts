import { Injectable } from '@angular/core';
import {Post} from "../models/post";

@Injectable({
  providedIn: 'root'
})
export class PostcreationService {

  public post: Post = new Post();

  constructor() {
    this.reset();
  }

  reset() {
    this.post.type = '';
    this.post.content = '';
  }
}
