import { TestBed } from '@angular/core/testing';

import { VoteSessionService } from './vote-session.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Tag } from '../models/tag';
import { PostTypes } from '../components/enums/postTypeEnum';
import { UserService } from './user.service';
import { Session } from '../models/session';

describe('VoteSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService]
  }));

  it('should be created', () => {
    const service: VoteSessionService = TestBed.get(VoteSessionService);
    expect(service).toBeTruthy();
  });

  it('should get vote session', () => {
    const service: VoteSessionService = TestBed.get(VoteSessionService);
    const userService: UserService = TestBed.get(UserService);
    const tagOne: Tag = new Tag();
    const tagTwo: Tag = new Tag();
    userService.session = new Session();
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.getVotesession([tagOne, tagTwo], PostTypes.TEXT);

    expect(val === undefined).toBe(false);
  });

  it('should return undefined if not logged in', () => {
    const service: VoteSessionService = TestBed.get(VoteSessionService);
    const tagOne: Tag = new Tag();
    const tagTwo: Tag = new Tag();
    const val = service.getVotesession([tagOne, tagTwo], PostTypes.TEXT);

    expect(val).toBe(undefined);
  });

  it('should allow to vote', () => {
    const service: VoteSessionService = TestBed.get(VoteSessionService);
    const userService: UserService = TestBed.get(UserService);
    const tagOne: Tag = new Tag();
    const tagTwo: Tag = new Tag();
    userService.session = new Session();
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.votePost('vote', 0);

    expect(val === undefined).toBe(false);
  });

  it('should return undefined on vote if not logged in', () => {
    const service: VoteSessionService = TestBed.get(VoteSessionService);
    const tagOne: Tag = new Tag();
    const tagTwo: Tag = new Tag();
    const val = service.votePost('vote', 0);

    expect(val).toBe(undefined);
  });
});
