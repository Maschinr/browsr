import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Session } from '../models/session';
import { SecurityContext } from '@angular/core';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService]
  }));

  afterAll(() => {
    const cookieService: CookieService = TestBed.get(CookieService);
    cookieService.deleteAll();
  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should login if Cookie exists', () => {
    const cookieService: CookieService = TestBed.get(CookieService);
    cookieService.deleteAll();
    const session: Session = new Session();
    session.sessionkey = 'key';
    cookieService.set('browsRSession', JSON.stringify(session));
    const service: UserService = TestBed.get(UserService);

    expect(service.session).toBe(undefined);
  });

  it('should logout', () => {
    const cookieService: CookieService = TestBed.get(CookieService);
    cookieService.deleteAll();
    const session: Session = new Session();
    session.sessionkey = 'key';
    const service: UserService = TestBed.get(UserService);
    service.logout();
  });


  it('should search user pattern', () => {
    const service: UserService = TestBed.get(UserService);
    service.searchUserPattern('pattern');
    expect(service).toBeTruthy();
  });
});
