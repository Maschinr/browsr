import { Injectable } from '@angular/core';
import { Tag } from '../models/tag';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public getAll(): Observable<Tag[]> {
    return this.http.get<Tag[]>('https://localhost:3000/api/tags');
  }

  public remove(name: string) {
    const model = {
      sessionkey: this.userService.session.sessionkey,
      user: this.userService.session.username,
      name
    };
    return this.http.post('https://localhost:3000/api/tags/remove', model);
  }

  public create(name: string) {
    const model = {
      sessionkey: this.userService.session.sessionkey,
      user: this.userService.session.username,
      name
    };
    return this.http.post('https://localhost:3000/api/tags/create', model);
  }
}
