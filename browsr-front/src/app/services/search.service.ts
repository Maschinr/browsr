import { Injectable } from '@angular/core';
import {Post} from "../models/post";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  search(param: string) {
    const query: string = '?search=' + param
    return this.http.get<Array<any>>('https://localhost:3000/api/search' + query);
  }
}
