import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Report } from "../models/report";
import { Post } from "../models/post";
import { UserService } from "./user.service";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient, private userService: UserService) { }

  public getAllReports(): Observable<any> {
    if (this.userService.session !== undefined) {
      return this.http.get<any[]>('https://localhost:3000/api/reports?user=' + this.userService.session.username +
        '&sessionkey=' + this.userService.session.sessionkey);
    } else {
      return undefined;
    }

  }

  public createReport(type: string, reason: string, id: number) {
    const model = {
      type,
      reason,
      id,
      user: this.userService.session.username,
      sessionkey: this.userService.session.sessionkey
    };

    return this.http.post('https://localhost:3000/api/reports/create', model);
  }

  public deleteReport(id: number) {
    const model = {
      id,
      user: this.userService.session.username,
      sessionkey: this.userService.session.sessionkey
    };

    return this.http.post('https://localhost:3000/api/reports/delete', model);
  }
}
