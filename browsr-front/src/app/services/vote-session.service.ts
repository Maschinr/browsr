import { Injectable } from '@angular/core';
import {VoteSession} from "../models/vote-session";
import {UserService} from "./user.service";
import {HttpClient} from "@angular/common/http";
import {PostTypes} from "../components/enums/postTypeEnum";
import {Tag} from "../models/tag";

@Injectable({
  providedIn: 'root'
})
export class VoteSessionService {

  constructor(private http: HttpClient, private userService: UserService) {

  }

  getVotesession(tags: Array<Tag>, type: PostTypes) {
    if (this.userService.isLoggedIn() === false) {
      return undefined;
    }

    let tagList = '';
    tags.forEach(n => {
      tagList += '&tags=';
      tagList += n.name;
    });

    return this.http.get<VoteSession>('https://localhost:3000/api/vote?user=' + this.userService.session.username + '&sessionkey=' +
      this.userService.session.sessionkey + tagList + '&type=' + type);
  }

  votePost(postid: string, votesessionid: number) {
    if (this.userService.isLoggedIn() === false) {
      return undefined;
    }

    const data = {
      user: this.userService.session.username,
      sessionkey: this.userService.session.sessionkey,
      voteSessionId: votesessionid,
      voted: postid
    };

    return this.http.post('https://localhost:3000/api/vote', data);
  }
}
