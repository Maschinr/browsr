import { Injectable } from '@angular/core';
import { Session } from '../models/session';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post';
import { callbackify } from 'util';
import { Comment } from '../models/comment';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    session: Session;

    public user: User = new User();

    constructor(private http: HttpClient, private cookieService: CookieService) {
        //Load the default cockie if it exists
        //Needs a function inside the server that validates a sessionID.

        if (this.cookieService.check('browsRSession')) {
            const cookie = this.cookieService.get('browsRSession');

            if (cookie != null) {
                const tempSession = JSON.parse(cookie);
                if (tempSession != null) {
                    //Validate if the Session Timestamp isnt too old.
                    this.session = tempSession[0];
                }
            }
        }
    }

    public isLoggedIn(): boolean {
        return (this.session != null);
    }

    public login(username: string, password: string, callback) {
        const loginData = {
            username,
            password
        };

        return this.http.post('https://localhost:3000/api/account/login', loginData)
            .subscribe(data => {
                const o: Session[] = JSON.parse(JSON.stringify(data));
                this.session = o[0];
                this.cookieService.set('browsRSession', JSON.stringify(o));
                callback(true);
            },
                err => {
                    console.log("Login fehlgeschlagen");
                    this.session = null;
                    callback(false);
                });

    }

    public changeEmail(sessionkey: string, password: string, oldemail: string, newemail: string, callback) {
        const emailData = {
            sessionkey,
            password,
            oldemail,
            newemail
        };

        this.http.post('https://localhost:3000/api/account/changeemail', emailData)
            .subscribe(data => {
            }, err => {
                if (err.status === 200) {
                    callback(true);

                }
                else {

                    callback(false);
                }
            })
    }

    public changePassword(sessionkey: string, oldpassword: string, newpassword: string, callback) {
        const passwordData = {
            sessionkey,
            oldpassword,
            newpassword
        };

        this.http.post('https://localhost:3000/api/account/changepassword', passwordData)
            .subscribe(data => {

            }, err => {
                if (err.status === 200) {
                    callback(true);
                }
                else {
                    callback(false);
                }
            })
    }

    public changeUsername(sessionkey: string, password: string, newname: string, callback) {
        const userData = {
            sessionkey,
            password,
            newname
        };

        this.http.post('https://localhost:3000/api/account/changename', userData)
            .subscribe(data => {

            }, err => {
                if (err.status === 200) {
                    callback(true);
                } else {
                    callback(false);
                }
            })
    }

    public deleteAccount(sessionkey: string, password: string, email: string, callback) {
        const deleteData = {
            sessionkey,
            password,
            email
        }

        this.http.post('https://localhost:3000/api/account/deleteaccount', deleteData)
            .subscribe(data => {

            }, err => {
                if (err.status === 200) {
                    callback(true);
                    this.logout();
                } else {
                    callback(false);
                }
            })
    }

    public getProfileInfos(username: string) {
        return this.http.get<User[]>('https://localhost:3000/api/account/getuser?exactUsername=' + username);
    }

    public getPostsFromUser(username: string) {
        return this.http.get<Post[]>('https://localhost:3000/api/posts/user?user=' + username);
    }

    public getCommentsFromUser(userid: number) {
        return this.http.get<Comment[]>('https://localhost:3000/api/comment/getcommentsfromuser?userid=' + userid);
    }

    public logout() {
        this.session = null;
        if (this.cookieService !== undefined) {
            this.cookieService.deleteAll();
        }

    }

    public register(username: string, email: string, password: string, callback) {
        const registerData = {
            username,
            email,
            password
        };

        this.http.post('https://localhost:3000/api/account/register', registerData)
            .subscribe(() => {
                callback(true);
            },
                error => {
                    callback(false);
                });
    }

    public searchUserPattern(pattern: string) {
        return this.http.get<User[]>('https://localhost:3000/api/account/getuser?usernamePattern=' + pattern);
    }


}
