import { TestBed, inject } from '@angular/core/testing';

import { PostService } from './post.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { HttpTestingController } from '@angular/common/http/testing';
import { ListType } from '../components/enums/listTypeEnum';
import { Direction } from '../components/enums/directionEnum';
import { Criteria } from '../components/enums/criteriaEnum';
import { UserService } from './user.service';
import { Session } from '../models/session';
import { Observable } from 'rxjs';
import { Post } from '../models/post';
import { Tag } from '../models/tag';
import { PromiseType } from 'protractor/built/plugins';
import { Ratings } from '../components/enums/ratingEnum';

describe('PostService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService, UserService]
  }));

  it('should be created', () => {
    const service: PostService = TestBed.get(PostService);
    expect(service).toBeTruthy();
  });

  it('should build a tag query', () => {
    const service: PostService = TestBed.get(PostService);
    service.getAll(ListType.HIGHLIGHT, 'user', 'tagname', 0,
      Direction.ASCENDING, 100, ['tagOne', 'tagTwo'], Criteria.POINTS);
  });

  it('should look into the archive', () => {
    const service: PostService = TestBed.get(PostService);
    service.getAll(ListType.ARCHIVE, 'user', 'tagname', 0,
      Direction.ASCENDING, 100, ['tagOne', 'tagTwo'], Criteria.POINTS);
  });

  it('should return nothing when user is empty', () => {
    const service: PostService = TestBed.get(PostService);
    const a = service.getAll(ListType.USER, '', 'tagname', 0,
      Direction.ASCENDING, 100, ['tagOne', 'tagTwo'], Criteria.POINTS);
    expect(a).toBe(undefined);
  });

  it('should look for a user list', () => {
    const service: PostService = TestBed.get(PostService);
    service.getAll(ListType.USER, 'user', 'tagname', 0,
      Direction.ASCENDING, 100, ['tagOne', 'tagTwo'], Criteria.POINTS);
  });

  it('should look for a list of one tagname', () => {
    const service: PostService = TestBed.get(PostService);
    service.getAll(ListType.TAG, 'user', 'tagname', 0,
      Direction.ASCENDING, 100, ['tagOne', 'tagTwo'], Criteria.POINTS);
  });

  it('should return undifiened if user is not logged in', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = null;
    const val = service.deletePost(1);
    expect(val).toBe(undefined);
  });

  it('should delete if user is logged in', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 0;
    userService.session.username = 'Username';
    const val = service.deletePost(1);
    expect(val === undefined).toBe(false);
  });

  it('should throw on undefined new post', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    expect(function () { service.createPost(undefined); }).toThrowError('Invalid Post');
  });

  it('should throw not logged in on new Post', () => {
    const service: PostService = TestBed.get(PostService);
    expect(function () { service.createPost(undefined); }).toThrowError('Not logged in');
  });

  it('should throw on invalid post', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    const post: Post = new Post();
    post.title = undefined;
    expect(function () { service.createPost(post); }).toThrowError('Invalid Post');
  });

  it('should create a post when valid', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const post: Post = new Post();
    post.title = 'title';
    post.type = 'TEXT';
    post.stag = new Tag();
    post.stag.name = 'stag';
    post.ptag = new Tag();
    post.ptag.name = 'ptag';
    post.content = 'content';
    const val = service.createPost(post);
    expect(val === undefined).toBe(false);
  });

  it('should rate a post', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.ratePost(1, Ratings.UP);
    expect(val === undefined).toBe(false);
  });

  it('should write a comment on a post', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.writeComment(1, 'comment');
    expect(val === undefined).toBe(false);
  });

  it('should delete a comment', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.deleteComment(1);
    expect(val === undefined).toBe(false);
  });

  it('should rate a post', () => {
    const service: PostService = TestBed.get(PostService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.deleteComment(1);
    expect(val === undefined).toBe(false);
  });

  it('should get post by id', () => {
    const service: PostService = TestBed.get(PostService);
    const val = service.getPostById(1);
    expect(val === undefined).toBe(false);
  });

  it('should get comments', () => {
    const service: PostService = TestBed.get(PostService);
    const val = service.getComments(1);
    expect(val === undefined).toBe(false);
  });
});
