import { TestBed } from '@angular/core/testing';

import { TagService } from './tag.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './user.service';
import { Session } from '../models/session';

describe('TagService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService]
  }));

  it('should be created', () => {
    const service: TagService = TestBed.get(TagService);
    expect(service).toBeTruthy();
  });

  it('should remove a tag', () => {
    const service: TagService = TestBed.get(TagService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.remove('End me pls');
    expect(val === undefined).toBe(false);
  });

  it('should create a tag', () => {
    const service: TagService = TestBed.get(TagService);
    const userService: UserService = TestBed.get(UserService);
    userService.session = new Session();
    userService.session._uid = 1;
    userService.session.sessionkey = 'key';
    userService.session.username = 'username';
    const val = service.create('i cant take it anymore');
    expect(val === undefined).toBe(false);
  });
});
