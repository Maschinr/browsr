import { TestBed } from '@angular/core/testing';

import { PostcreationService } from './postcreation.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

describe('PostcreationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CookieService]
  }));

  it('should be created', () => {
    const service: PostcreationService = TestBed.get(PostcreationService);
    expect(service).toBeTruthy();
  });
});
