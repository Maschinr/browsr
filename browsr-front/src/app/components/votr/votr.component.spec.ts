import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotrComponent } from './votr.component';
import { CookieService } from 'ngx-cookie-service';
import { EmbedVideoService } from 'ngx-embed-video';
import { PostComponent } from '../post/post.component';
import { InfoboxComponent } from '../infobox/infobox.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { TagService } from 'src/app/services/tag.service';
import { of, throwError } from 'rxjs';
import { Tag } from 'src/app/models/tag';
import { VoteSessionService } from 'src/app/services/vote-session.service';
import { PostTypes } from '../enums/postTypeEnum';
import { VoteSession } from 'src/app/models/vote-session';

describe('VotrComponent', () => {
  let component: VotrComponent;
  let fixture: ComponentFixture<VotrComponent>;
  let tagService: TagService;
  let voteService: VoteSessionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService, EmbedVideoService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tagService = fixture.debugElement.injector.get(TagService);
    voteService = fixture.debugElement.injector.get(VoteSessionService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use the Tag Service', () => {
    const spy = spyOn(tagService, 'getAll').and.returnValue(of([new Tag()]));
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  });

  it('should react on tag change', () => {
    const tagOne = new Tag();
    const tagTwo = new Tag();
    const spy = spyOn(component, 'buildVote').and.returnValue(null);
    component.selectedType = PostTypes.TEXT;
    component.onTagChange([tagOne, tagTwo]);
    expect(spy).toHaveBeenCalled();
  });

  it('should react on type change', () => {
    const tagOne = new Tag();
    const tagTwo = new Tag();
    component.selectedTags = [tagOne, tagTwo];
    const spy = spyOn(component, 'buildVote').and.returnValue(null);
    component.onTypeChange('TEXT');
    expect(component.selectedType.toString()).toBe('TEXT');
    expect(spy).toHaveBeenCalled();
  });

  it('should build a voting', () => {
    const tagOne = new Tag();
    const tagTwo = new Tag();
    const session = new VoteSession();
    const spy = spyOn(voteService, 'getVotesession').and.returnValue(of(session));
    component.buildVote([tagOne, tagTwo], PostTypes.TEXT);
    expect(spy).toHaveBeenCalled();
    expect(component.curSession).toBe(session);
  });

  it('should handle errors in voting sessions', () => {
    const tagOne = new Tag();
    const tagTwo = new Tag();
    const session = new VoteSession();
    const spy = spyOn(voteService, 'getVotesession').and.returnValue(throwError({ status: 401 }));
    component.buildVote([tagOne, tagTwo], PostTypes.TEXT);
    expect(spy).toHaveBeenCalled();
  });

  it('should show error on voting', () => {
    const session = new VoteSession();
    session.id = 0;
    component.curSession = session;
    const spy = spyOn(voteService, 'votePost').and.returnValue(throwError({ status: 401 }));
    component.votePost('1');
    expect(spy).toHaveBeenCalled();
  });

  it('should vote post success', () => {
    const session = new VoteSession();
    session.id = 0;
    component.curSession = session;
    const spy = spyOn(voteService, 'votePost').and.returnValue(of({ status: 200 }));
    component.votePost('1');
    expect(spy).toHaveBeenCalled();
  });
});
