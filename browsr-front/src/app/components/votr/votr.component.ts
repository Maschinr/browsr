import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { TagService } from '../../services/tag.service';
import { VoteSessionService } from '../../services/vote-session.service';
import { VoteSession } from '../../models/vote-session';
import { Tag } from '../../models/tag';
import { PostTypes } from '../enums/postTypeEnum';
import { Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { EmbedVideoService } from "ngx-embed-video/dist";

@Component({
  selector: 'app-votr',
  templateUrl: './votr.component.html',
  styleUrls: ['./votr.component.scss']
})
export class VotrComponent implements OnInit {

  public voted = false;
  public curSession: VoteSession;
  public postTypeList: Array<string>;
  tagList: Array<Tag>;
  selectedTags = Array<Tag>();
  selectedType: PostTypes;
  public hasError: Boolean = false;
  public postEnum = PostTypes;

  constructor(private embedService: EmbedVideoService, private userService: UserService, private voteService: VoteSessionService, private postService: PostService, private tagService: TagService, private router: Router) {
    if (!userService.isLoggedIn()) {
      router.navigate(['/notLoggedIn/votr']);
    }

    this.postTypeList = Object.keys(PostTypes);

    for (let i = 0; i < this.postTypeList.length; i++) {
      this.postTypeList[i] = this.postTypeList[i].toLowerCase();
      this.postTypeList[i] = this.postTypeList[i].charAt(0).toUpperCase() + this.postTypeList[i].slice(1);
    }
  }

  ngOnInit() {
    this.tagService.getAll().subscribe(data => {
      this.tagList = data;
    });
  }

  onTagChange(event) {
    if (event !== undefined) {
      this.hasError = false;
      if (event.length === 2) {
        this.selectedTags = event;
        if (this.selectedTags !== undefined && this.selectedType !== undefined) {
          if (this.selectedTags.length === 2) {
            this.buildVote(this.selectedTags, this.selectedType);
          }
        }
      }
    }
  }

  onTypeChange(event) {
    if (event !== undefined) {
      this.hasError = false;
      this.selectedType = event.toUpperCase();
      if (this.selectedTags !== undefined && this.selectedType !== undefined) {
        if (this.selectedTags.length === 2) {
          this.buildVote(this.selectedTags, this.selectedType);
        }
      }
    }
  }

  buildVote(tags: Array<Tag>, type: PostTypes) {
    let res = this.voteService.getVotesession(tags, type);
    if (res === undefined) {
    } else {
      res.subscribe(data => {
        console.log(data);
        this.curSession = data;
      }, err => {
        this.hasError = true;
      });
    }
  }

  votePost(id: string) {
    this.voteService.votePost(id, this.curSession.id).subscribe(data => {
      console.log('Voted!');
      this.voted = true;
    }, err => {
      this.hasError = true;
    });
  }
}
