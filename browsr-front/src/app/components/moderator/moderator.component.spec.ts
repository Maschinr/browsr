import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeratorComponent } from './moderator.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { UserService } from 'src/app/services/user.service';
import { Session } from 'src/app/models/session';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from 'src/app/models/tag';
import { of, throwError } from 'rxjs';
import { ReportService } from 'src/app/services/report.service';
import { Report } from 'src/app/models/report';

describe('ModeratorComponent', () => {
  let component: ModeratorComponent;
  let fixture: ComponentFixture<ModeratorComponent>;
  let userService: UserService;
  let tagService: TagService;
  let reportService: ReportService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = fixture.debugElement.injector.get(UserService);
    tagService = fixture.debugElement.injector.get(TagService);
    reportService = fixture.debugElement.injector.get(ReportService);
  });


  it('should renavigate if not logged in as moderator', () => {
    expect(component).toBeTruthy();
  });


  it('should renavigate if user is not moderator', () => {
    fixture.ngZone.run(() => {
      userService.session = new Session();
      userService.session.role = 'USER';
      TestBed.createComponent(ModeratorComponent);
      fixture.detectChanges();
    });
  });

  it('should be able to load tags', () => {
    fixture.ngZone.run(() => {
      const tag = new Tag();
      tag.name = 'test';
      const spy = spyOn(tagService, 'getAll').and.returnValue(of([tag]));
      component.ngOnInit();
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should have a report observer that subscribes', () => {
    fixture.ngZone.run(() => {
      const report = new Report();
      const spy = spyOn(reportService, 'getAllReports').and.returnValue(of([report]));
      component.ngOnInit();
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should create a tag successful', () => {
    fixture.ngZone.run(() => {
      const tag = new Tag();
      const getspy = spyOn(tagService, 'getAll').and.returnValue(of([tag]));
      const spy = spyOn(tagService, 'create').and.returnValue(of({ status: 200 }));
      component.create('test');
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should show info of tag can not be added', () => {
    fixture.ngZone.run(() => {
      const spy = spyOn(tagService, 'create').and.returnValue(throwError({ status: 401 }));
      component.create('test');
      expect(component.info.modalOpen).toBe(true);
    });
  });

  it('should remove a tag successful', () => {
    fixture.ngZone.run(() => {
      const tag = new Tag();
      const getspy = spyOn(tagService, 'getAll').and.returnValue(of([tag]));
      const spy = spyOn(tagService, 'remove').and.returnValue(of({ status: 200 }));
      component.remove('test');
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should show info of tag can not be removed', () => {
    fixture.ngZone.run(() => {
      const spy = spyOn(tagService, 'remove').and.returnValue(throwError({ status: 401 }));
      component.remove('test');
      expect(component.info.modalOpen).toBe(true);
    });
  });

  it('should remove a report successful', () => {
    fixture.ngZone.run(() => {
      const getspy = spyOn(reportService, 'getAllReports').and.returnValue(of([]));
      const spy = spyOn(reportService, 'deleteReport').and.returnValue(of({ status: 200 }));
      component.removeReport(1);
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should show info of tag can not be removed', () => {
    fixture.ngZone.run(() => {
      const spy = spyOn(reportService, 'deleteReport').and.returnValue(throwError({ status: 401 }));
      component.removeReport(1);
      expect(component.info.modalOpen).toBe(true);
    });
  });


});
