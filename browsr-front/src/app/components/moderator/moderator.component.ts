import { Component, OnInit, ViewChild } from '@angular/core';
import { InfoboxComponent } from "../infobox/infobox.component";
import { PostcreationService } from "../../services/postcreation.service";
import { UserService } from "../../services/user.service";
import { PostService } from "../../services/post.service";
import { TagService } from "../../services/tag.service";
import { Router } from "@angular/router";
import { Tag } from "../../models/tag";
import { ReportService } from "../../services/report.service";

@Component({
  selector: 'app-moderator',
  templateUrl: './moderator.component.html',
  styleUrls: ['./moderator.component.scss']
})
export class ModeratorComponent implements OnInit {

  @ViewChild(InfoboxComponent) info: InfoboxComponent;
  public tagList: Array<Tag> = [];
  public reportList: Array<any> = [];

  constructor(private postCreationService: PostcreationService, private userService: UserService,
    private postService: PostService, private tagService: TagService, private router: Router,
    private reportService: ReportService) {
    if (!userService.isLoggedIn()) {
      router.navigate(['/notLoggedIn/moderator']);
      return;
    }

    if (userService.session.role !== 'MODERATOR') {
      router.navigate(['/notLoggedIn/moderator']);
      return;
    }
  }

  remove(name: string) {
    this.tagService.remove(name).subscribe(data => {
      this.info.view('Info', 'Tag removed successfully');
      this.tagService.getAll().subscribe(tags => {
        this.tagList = tags;
      });
    }, err => {
      this.info.view('Error', 'Error removing Tag');
    });
  }

  removeReport(id: number) {
    this.reportService.deleteReport(id).subscribe(data => {
      this.info.view('Info', 'Report removed successfully');
      this.reportService.getAllReports().subscribe(reports => {
        this.reportList = reports;
      });
    }, err => {
      this.info.view('Error', 'Error removing Report');
    });
  }

  create(name: string) {
    this.tagService.create(name).subscribe(data => {
      this.info.view('Info', 'Tag created successfully');
      this.tagService.getAll().subscribe(tags => {
        this.tagList = tags;
      });
    }, err => {
      this.info.view('Error', 'Error creating Tag \""name\"');
    });
  }

  ngOnInit() {
    const tagObserver = this.tagService.getAll();
    if (tagObserver !== undefined) {
      tagObserver.subscribe(tags => {
        this.tagList = tags;
      });
    }

    const reportObserver = this.reportService.getAllReports();
    if (reportObserver !== undefined) {
      reportObserver.subscribe(reports => {
        this.reportList = reports;
      });
    }

  }

}
