import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Post } from 'src/app/models/post';
import { ListType } from '../enums/listTypeEnum';
import { PostService } from '../../services/post.service';
import { Direction } from '../enums/directionEnum';
import { Criteria } from '../enums/criteriaEnum';
import { PostlistComponent } from '../postlist/postlist.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    public username: string = "";
    role: string = "";
    points: number = 0;
    user: User;
    posts: Post;
    id: number;
    isUser: boolean = false;
    isModerator: boolean = false;
    isPostActive: boolean = true;
    isCommentActive: boolean = false;
    public commentList: Array<any> = [];
    public postList: Array<Post> = [];
    public listTypeEnum = ListType;

    @ViewChild(PostlistComponent) apppost: PostlistComponent;

    constructor(private route: ActivatedRoute, private router: Router, public userservice: UserService, private postservice: PostService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.userservice.getProfileInfos(params['user']).subscribe(data => {
                if (data.length > 0) {
                    this.user = data[0];
                    this.username = this.user.username;
                    this.userservice.user.username = this.user.username;
                    this.role = this.user.role;
                    this.points = this.user.points;
                    this.isModerator = false;
                    this.isUser = false;
                    this.id = this.user._uid;

                    if (this.role == "USER") {
                        this.isUser = true;
                    }
                    if (this.role == "MODERATOR") {
                        this.isModerator = true;
                    }

                    this.userservice.getPostsFromUser(this.username).subscribe(posts => {
                        this.postList = posts;
                    })

                    this.userservice.getCommentsFromUser(this.id).subscribe(comments => {
                        this.commentList = comments;
                    });
                } else {
                    this.router.navigate(["/highlight"]);
                }
            });
        });
    }


    showPosts() {
        this.isPostActive = true;
        this.isCommentActive = false;
    }

    showComments() {
        this.isCommentActive = true;
        this.isPostActive = false;
    }

}
