import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';
import { Post } from 'src/app/models/post';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    fixture.ngZone.run(() => {
      expect(component).toBeTruthy();
    });
  });

  it('should redirect on invalid user', () => {
    fixture.ngZone.run(() => {
      const spy = spyOn(service, 'getProfileInfos').and.returnValue(of([]));
      component.ngOnInit();
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should be able to load a user', () => {
    fixture.ngZone.run(() => {
      const user = new User();
      user._uid = 0;
      user.email = 'mail@domain.abc';
      user.points = 0;
      user.role = 'USER';
      user.username = 'username';
      const spy = spyOn(service, 'getProfileInfos').and.returnValue(of([user]));
      const postSpy = spyOn(service, 'getPostsFromUser').and.returnValue(of([new Post()]));
      const commentSpy = spyOn(service, 'getCommentsFromUser').and.returnValue(of([]));
      component.ngOnInit();
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
      expect(component.isUser).toBe(true);
    });
  });

  it('should be able to load a mod', () => {
    fixture.ngZone.run(() => {
      const user = new User();
      user._uid = 0;
      user.email = 'mail@domain.abc';
      user.points = 0;
      user.role = 'MODERATOR';
      user.username = 'username';
      const spy = spyOn(service, 'getProfileInfos').and.returnValue(of([user]));
      component.ngOnInit();
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
      expect(component.isModerator).toBe(true);
    });
  });

  it('should display posts', () => {
    component.showPosts();
    expect(component.isPostActive).toBe(true);
  });

  it('should show comments', () => {
    component.showComments();
    expect(component.isCommentActive).toBe(true);
  });
});
