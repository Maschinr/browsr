import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostlistComponent } from './postlist.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { Tag } from 'src/app/models/tag';
import { PostService } from 'src/app/services/post.service';
import { of } from 'rxjs';
import { Criteria } from '../enums/criteriaEnum';
import { Direction } from '../enums/directionEnum';
import { Post } from 'src/app/models/post';
import { TagService } from 'src/app/services/tag.service';

describe('PostlistComponent', () => {
  let component: PostlistComponent;
  let fixture: ComponentFixture<PostlistComponent>;
  let postService: PostService;
  let tagService: TagService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    postService = fixture.debugElement.injector.get(PostService);
    tagService = fixture.debugElement.injector.get(TagService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change on filter', () => {
    const spy = spyOn(postService, 'getAll').and.returnValue(of([]));
    const tag = new Tag();
    tag.name = 'test';
    component.onChange([tag]);
    expect(spy).toHaveBeenCalled();
  });

  it('should order Descending', () => {
    component.direction = 'Descending';
    component.parseCrit();
    expect(component.realDirection === Direction.DESCENDING).toBe(true);
  });

  it('should order by time', () => {
    component.criteria = 'Points';
    component.parseCrit();
    expect(component.realCriteria).toBe(Criteria.POINTS);
  });

  it('should set username', () => {
    component.setUserName('name');
    expect(component.username).toBe('name');
  });

  it('should call for new list after change', () => {
    const post = new Post();
    post.id = 0;
    post.title = 'test';
    const spy = spyOn(postService, 'getAll').and.returnValue(of([post]));
    component.onChangeCrit(null);
    expect(spy).toHaveBeenCalled();
  });

  it('should init with services', () => {
    const post = new Post();
    const tag = new Tag();
    post.id = 0;
    post.title = 'test';
    const spy = spyOn(postService, 'getAll').and.returnValue(of([post]));
    const tagSpy = spyOn(tagService, 'getAll').and.returnValue(of([tag]));
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

});
