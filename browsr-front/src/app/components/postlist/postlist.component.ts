import { Component, Input, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from '../../models/tag';
import { ListType } from "../enums/listTypeEnum";
import { Criteria } from "../enums/criteriaEnum";
import { Direction } from "../enums/directionEnum";

@Component({
    selector: 'app-postlist',
    templateUrl: './postlist.component.html',
    styleUrls: ['./postlist.component.scss'],
    providers: [PostService, TagService]
})
export class PostlistComponent implements OnInit {

    @Input() listType: ListType;
    private tagName: string;

    public postList: Array<Post> = [];
    public tagList: Array<Tag> = [];
    public listEnum = ListType;
    public criteriaEnum = Criteria;
    public criteria = 'Time';
    public realCriteria = Criteria.TIME;
    public direction = 'Ascending';
    public realDirection = Direction.ASCENDING;
    public tags = [];
    public username: string;

    //TODO limit and start inf scroll or pages

    setTagName(name: string) {
      this.tagName = name;
      this.parseCrit();
      const observer = this.postService.getAll(this.listType, '', this.tagName, 0, this.realDirection, 100, this.tags, this.realCriteria);
      observer.subscribe(posts => {
        this.postList = posts;
        console.log(this.postList);
      });
    }


    parseCrit() {
        if (this.direction === 'Ascending') {
            this.realDirection = Direction.ASCENDING;
        } else if (this.direction === 'Descending') {
            this.realDirection = Direction.DESCENDING;
        }

        if (this.criteria === 'Time') {
            this.realCriteria = Criteria.TIME;
        } else if (this.criteria === 'Points') {
            this.realCriteria = Criteria.POINTS;
        }
    }

    onChange(event) {
        this.parseCrit();
        for (let i = 0; i < event.length; i++) {
            event[i] = event[i].name;
        }
        const observer = this.postService.getAll(this.listType, '', this.tagName, 0, this.realDirection, 100, event, this.realCriteria);
        if (observer !== undefined) {
            observer.subscribe(posts => {
                this.postList = posts;
            });
            this.tags = event;
        }

    }

    onChangeCrit(event) {
        this.parseCrit();
        const observer = this.postService.getAll(this.listType, '', this.tagName, 0, this.realDirection, 100, this.tags, this.realCriteria);
        observer.subscribe(posts => {
            this.postList = posts;
        });
    }

    constructor(private postService: PostService, private tagService: TagService) {

    }

    setUserName(username: string) {
        this.username = username;
        this.onChange([]);
    }

    ngOnInit() {
        if (this.listType === undefined) {
            console.error('No listType given!');
        }

        const tagObserver = this.tagService.getAll();

        if (tagObserver !== undefined) {
            this.tagService.getAll().subscribe(tags => {
                this.tagList = tags;
            });
        }

        const postObserver = this.postService.getAll(this.listType, '', this.tagName, 0,
            this.realDirection, 100, this.tags, this.realCriteria);
        if (postObserver !== undefined) {
            if (postObserver !== undefined) {
                postObserver.subscribe(posts => {
                    this.postList = posts;
                });
            }
        }
    }



}
