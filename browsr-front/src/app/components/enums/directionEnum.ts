export enum Direction {
  ASCENDING = -1,
  DESCENDING = 1
}
