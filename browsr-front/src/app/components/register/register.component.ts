import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from "../../services/user.service";
import * as EmailValidator from 'node_modules/email-validator';
import { InfoboxComponent } from '../infobox/infobox.component';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {



    modalOpen: boolean = false;
    alert = true;
    registrationSuccessful: boolean = false;

    passwordFormat: boolean = false;
    mailFormat: boolean = false;

    @ViewChild(InfoboxComponent) info: InfoboxComponent;

    constructor(private userService: UserService) { }

    ngOnInit() {
    }

    view() {
        this.modalOpen = true;
    }

    register(username: string, mail: string, password: string, password_again: string) {
        this.passwordFormat = false;
        this.mailFormat = false;
        if (password !== password_again) {
            this.passwordFormat = true;
        }

        if (!EmailValidator.validate(mail)) {
            this.mailFormat = true;
        }

        if (this.passwordFormat === false && this.mailFormat === false) {
            this.userService.register(username, mail, password, (bool: boolean) => { this.onRegister(bool, username, password); });

        }

    }

    onRegister(val: boolean, username: string, password: string) {
        this.alert = val;
        if (val === true) {
            this.modalOpen = false;
        } else {
            this.info.view('Registration successful', 'Thanks for your registration!');
            this.userService.login(username, password, (bool: boolean) => { });
        }
    }

}
