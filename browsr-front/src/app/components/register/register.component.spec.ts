import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should view', () => {
    component.view();
    expect(component.modalOpen).toBe(true);
  });

  it('should warn invalid mail format', () => {
    component.register('username', 'invalidmail', 'pass', 'pass');
    expect(component.mailFormat).toBe(true);
  });

  it('should warn invalid password', () => {
    component.register('username', 'validmail@gmail.com', 'pass', 'notpass');
    expect(component.passwordFormat).toBe(true);
  });

  it('should register succesful', () => {
    const spy = spyOn(service, 'register').and.returnValue(of({ status: 200 }));

    component.register('username', 'validmail@gmail.com', 'pass', 'pass');
  });

  it('should login after successful registration', () => {
    const loginSpy = spyOn(service, 'login').and.returnValue(of({ status: 200 }));
    component.onRegister(false, 'username', 'password');
    expect(loginSpy).toHaveBeenCalled();
  });

  it('should close if not successful', () => {
    component.onRegister(true, 'username', 'password');
    expect(component.modalOpen).toBe(false);
  });

  it('should callback on register', () => {
    const spy = spyOn(service, 'register').and.callThrough();
    component.register('username', 'mail@domain.com', 'pass', 'pass');
    expect(spy).toHaveBeenCalled();
  });

});
