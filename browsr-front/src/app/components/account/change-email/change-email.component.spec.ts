import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeEmailComponent } from './change-email.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { PostService } from 'src/app/services/post.service';
import { HttpClient } from '@angular/common/http';
import { PostcreationService } from 'src/app/services/postcreation.service';
import { UserService } from 'src/app/services/user.service';
import { DebugElement } from '@angular/core';
import { User } from 'src/app/models/user';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Session } from 'src/app/models/session';
import { of, throwError } from 'rxjs';

describe('ChangeEmailComponent', () => {
  let component: ChangeEmailComponent;
  let fixture: ComponentFixture<ChangeEmailComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService, UserService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEmailComponent);
    service = fixture.debugElement.injector.get(UserService)
    component = fixture.componentInstance;
    fixture.detectChanges();


  });

  it('Should go to not logged in component', () => {
    expect(component).toBeTruthy();
  });

  it('Should send change Mail request', () => {
    const spy = spyOn(service, 'changeEmail').and.returnValue(throwError({ status: 200 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeEmail('testpass', 'oldmail', 'newmail');
    expect(spy).toHaveBeenCalled();
  });

  it('Should fail change Mail request', () => {
    const spy = spyOn(service, 'changeEmail').and.returnValue(throwError({ status: 401 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeEmail('testpass', 'oldmail', 'newmail');
    expect(spy).toHaveBeenCalled();
  });

  it('Should run into callback', () => {
    const spy = spyOn(service, 'changeEmail').and.callThrough();
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeEmail('testpass', 'oldmail', 'newmail');
    expect(spy).toHaveBeenCalled();
  });

  it('should show message on success', () => {
    component.onChangeMail(true);
    expect(component.success).toBe(true);
  });

  it('should show message on failed', () => {
    component.onChangeMail(false);
    expect(component.success).toBe(false);
  });

});
