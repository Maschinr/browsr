import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-email',
    templateUrl: './change-email.component.html',
    styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

    success: boolean = false;
    alert: boolean = false;

    constructor(private userService: UserService, private router: Router) {
        if (!userService.isLoggedIn()) {
            router.navigate(['/notLoggedIn/account']);
        }
    }

    ngOnInit() {
    }

    changeEmail(password: string, oldemail: string, newemail: string) {
        this.userService.changeEmail(this.userService.session.sessionkey, password, oldemail, newemail, (i) => { this.onChangeMail(i); });

    }

    onChangeMail(val: boolean) {
        this.success = val;
        this.alert = false;

        if (val == false) {
            this.alert = true;
            this.success = false;
        }
    }

}
