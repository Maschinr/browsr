import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordComponent } from './change-password.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Session } from 'src/app/models/session';
import { of, throwError } from 'rxjs';

describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call change password', () => {
    const spy = spyOn(service, 'changePassword').and.returnValue(throwError({ status: 200 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changePassword('oldpass', 'newpass', 'newpass');
    expect(spy).toHaveBeenCalled();
  });

  it('should run into callback', () => {
    const spy = spyOn(service, 'changePassword').and.callThrough();
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changePassword('oldpass', 'newpass', 'newpass');
    expect(spy).toHaveBeenCalled();
  });

  it('should fail change password', () => {
    const spy = spyOn(service, 'changePassword').and.returnValue(throwError({ status: 401 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changePassword('oldpass', 'newpass', 'newpass');
    expect(spy).toHaveBeenCalled();
  });

  it('should show message on success', () => {
    component.onPasswordChange(true);
    expect(component.success).toBe(true);
  });

  it('should show message on failed', () => {
    component.onPasswordChange(false);
    expect(component.success).toBe(false);
  });

  it('should alert when passwords dont match', () => {
    component.changePassword('oldpass', 'newpass', 'random');
    expect(component.nomatch).toBe(true);
  });
});
