import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

    success: boolean = false;
    alert: boolean = false;
    nomatch: boolean = false;

    constructor(private userservice: UserService, private router: Router) {
        if (!userservice.isLoggedIn()) {
            router.navigate(['/notLoggedIn/account']);
        }
    }

    ngOnInit() {
    }

    changePassword(oldpassword: string, newpassword: string, newpassword_again: string) {
        if (newpassword === newpassword_again) {

            this.userservice.changePassword(this.userservice.session.sessionkey, oldpassword, newpassword, (i) => { this.onPasswordChange(i); });
        }
        else {
            this.nomatch = true;
            this.alert = false;
            this.success = false;
        }

    }

    onPasswordChange(val: boolean) {
        this.success = val;
        this.alert = false;
        this.nomatch = false;

        if (val == false) {
            this.alert = true;
            this.success = false;
            this.nomatch = false;
        }
    }
}
