import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-delete-account',
    templateUrl: './delete-account.component.html',
    styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent implements OnInit {

    success: boolean = false;
    alert: boolean = false;

    constructor(private userservice: UserService, private router: Router) {
        if (!userservice.isLoggedIn()) {
            router.navigate(['/notLoggedIn/account']);
        }
    }

    ngOnInit() {
    }

    deleteAccount(password: string, email: string) {
        this.userservice.deleteAccount(this.userservice.session.sessionkey, password, email, (i) => { this.onAccountDeletion(i); });
    }

    onAccountDeletion(val: boolean) {

        if (val == false) {
            this.alert = true;
            this.success = false;
        }
        else {
            this.alert = false;
            this.success = true;
            setTimeout(() => {
                this.router.navigate(["/highlight"]);
            }, 5000);
        }
    }

}
