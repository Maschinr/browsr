import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAccountComponent } from './delete-account.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { UserService } from 'src/app/services/user.service';
import { Session } from 'src/app/models/session';
import { of, throwError } from 'rxjs';

describe('DeleteAccountComponent', () => {
  let component: DeleteAccountComponent;
  let fixture: ComponentFixture<DeleteAccountComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send delete Account request', () => {
    const spy = spyOn(service, 'deleteAccount').and.returnValue(throwError({ status: 200 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.deleteAccount('password', 'mail');
    expect(spy).toHaveBeenCalled();
  });

  it('should run into callback', () => {
    const spy = spyOn(service, 'deleteAccount').and.callThrough();
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.deleteAccount('password', 'mail');
    expect(spy).toHaveBeenCalled();
  });

  it('should fail delete Account request', () => {
    const spy = spyOn(service, 'deleteAccount').and.returnValue(throwError({ status: 401 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.deleteAccount('password', 'mail');
    expect(spy).toHaveBeenCalled();
  });

  it('should redirect after success', () => {
    component.onAccountDeletion(true);
    expect(component.success).toBe(true);
  });

  it('should alert on fail', () => {
    component.onAccountDeletion(false);
    expect(component.success).toBe(false);
  });
});
