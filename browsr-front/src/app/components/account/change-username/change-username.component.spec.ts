import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUsernameComponent } from './change-username.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { UserService } from 'src/app/services/user.service';
import { Session } from 'src/app/models/session';
import { of, throwError } from 'rxjs';

describe('ChangeUsernameComponent', () => {
  let component: ChangeUsernameComponent;
  let fixture: ComponentFixture<ChangeUsernameComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUsernameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send change username request', () => {
    const spy = spyOn(service, 'changeUsername').and.returnValue(throwError({ status: 200 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeUsername('password', 'newusername');
    expect(spy).toHaveBeenCalled();
  });

  it('should run into callbacl', () => {
    const spy = spyOn(service, 'changeUsername').and.callThrough()
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeUsername('password', 'newusername');
    expect(spy).toHaveBeenCalled();
  });

  it('should fail change username request', () => {
    const spy = spyOn(service, 'changeUsername').and.returnValue(throwError({ status: 401 }));
    service.session = new Session();
    service.session._uid = 1;
    service.session.email = 'oldmail';
    service.session.role = 'USER';
    service.session.sessionkey = 'key';
    service.session.timestamp = '';
    service.session.username = 'user';
    component.changeUsername('password', 'newusername');
    expect(spy).toHaveBeenCalled();
  });

  it('should show message on success', () => {
    component.onUsernameChange(true);
    expect(component.success).toBe(true);
  });

  it('should show message on failed', () => {
    component.onUsernameChange(false);
    expect(component.success).toBe(false);
  });
});
