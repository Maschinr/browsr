import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-username',
    templateUrl: './change-username.component.html',
    styleUrls: ['./change-username.component.scss']
})
export class ChangeUsernameComponent implements OnInit {

    success: boolean = false;
    alert: boolean = false;

    constructor(private userservice: UserService, private router: Router) {
        if (!userservice.isLoggedIn()) {
            router.navigate(['/notLoggedIn/account']);
        }
    }

    ngOnInit() {
    }

    changeUsername(password: string, newname: string) {

        this.userservice.changeUsername(this.userservice.session.sessionkey, password, newname, (i) => { this.onUsernameChange(i); });
    }

    onUsernameChange(val: boolean) {
        this.success = val;
        this.alert = false;

        if (val == false) {
            this.alert = true;
            this.success = false;
        }
    }

}
