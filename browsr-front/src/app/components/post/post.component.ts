import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Post } from 'src/app/models/post';
import { TagService } from 'src/app/services/tag.service';
import { PostService } from 'src/app/services/post.service';
import { Ratings } from '../enums/ratingEnum';
import { PostTypes } from '../enums/postTypeEnum';
import { InfoboxComponent } from '../infobox/infobox.component';
import { EmbedVideoService } from 'ngx-embed-video/dist';
import { Router } from '@angular/router';
import { ReportService } from '../../services/report.service';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {


  constructor(public userService: UserService, public reportService: ReportService, public tagService: TagService, public postService: PostService, public embedService: EmbedVideoService, public router: Router) {

  }

  @Input() post: Post;
  //active needs to be an input, so that when using it with the postdetail
  //we are able to auto expand this post.
  //Also in the future maybe users want to auto expand all to just need to scroll down the start page
  //without expanding every single post.
  @Input() active = false;
  burgerOpened = false;
  public postEnum = PostTypes;
  public rateEnum = Ratings;

  @ViewChild(InfoboxComponent) info: InfoboxComponent;

  ngOnInit() {
    if (this.post === undefined) {
      console.error('No post given!');
    }
  }

  rate(rating: Ratings) {
    const res = this.postService.ratePost(this.post.id, rating);
    if (res === undefined) {
      if (this.info !== undefined) {
        this.info.view('Error', 'You must be logged in to rate a Post');
      }
    } else {
      res.subscribe(obj => {
        this.post.points = obj.points;
      }, err => {
        if (this.info !== undefined) {
          this.info.view('Error', 'You already rated this Post with this Rating');
        }
      });
    }
  }

  report() {
    //TODO show report modal
    this.reportService.createReport('post', 'test', this.post.id).subscribe(data => {
      if (this.info !== undefined) {
        this.info.view('info', 'Post reported');
      }

    }, err => {
      if (this.info !== undefined) {
        this.info.view('Error', 'Error reporting post');
      }
    });
  }

  delete() {
    this.postService.deletePost(this.post.id).subscribe(data => {
      if (this.info !== undefined) {
        this.info.view('Success', 'The post is deleted, it will harm no one anymore');
      }
    }, err => {
      console.log(err);
      if (this.info !== undefined) {
        this.info.view('Error', 'Error deleting post');
      }
    });
  }
}
