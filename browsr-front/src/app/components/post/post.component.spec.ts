import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostComponent } from './post.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { Ratings } from '../enums/ratingEnum';
import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/services/post.service';
import { of, Observable, throwError } from 'rxjs';
import { ReportService } from 'src/app/services/report.service';

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;
  let service: PostService;
  let reportService: ReportService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(PostService);
    reportService = fixture.debugElement.injector.get(ReportService);
  });

  it('should create', () => {
    console.log('Create Post Component');
    console.log(component);
    expect(component).toBeTruthy();
  });

  it('can not rate when not logged in', () => {
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.rate(Ratings.UP);
  });

  it('should vote success', () => {
    const post = new Post();
    post.id = 0;
    component.post = post;
    const spy = spyOn(service, 'ratePost').and.returnValue(of({ points: 1 }));
    component.rate(Ratings.UP);
    expect(spy).toHaveBeenCalled();
    expect(component.post.points).toBe(1);
  });

  it('should return an error when not allowed to vote', () => {
    const post = new Post();
    post.id = 0;
    component.post = post;
    const spy = spyOn(service, 'ratePost').and.returnValue(throwError({ status: 401 }));
    component.rate(Ratings.UP);
    expect(spy).toHaveBeenCalled();

  });

  it('should be report', () => {
    const spy = spyOn(reportService, 'createReport').and.returnValue(of({ status: 200 }));
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.report();
    expect(spy).toHaveBeenCalled();
  });

  it('should show error when report not possible', () => {
    const spy = spyOn(reportService, 'createReport').and.returnValue(throwError({ status: 401 }));
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.report();
    expect(spy).toHaveBeenCalled();
    expect(component.info.modalOpen).toBe(true);
  });

  it('should delet success', () => {
    const spy = spyOn(service, 'deletePost').and.returnValue(of({ status: 200 }));
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.delete();
    expect(spy).toHaveBeenCalled();

  });

  it('should show message on delete fail', () => {
    const spy = spyOn(service, 'deletePost').and.returnValue(throwError({ status: 401 }));
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.delete();
    expect(spy).toHaveBeenCalled();
    expect(component.info.modalOpen).toBe(true);
  });

});
