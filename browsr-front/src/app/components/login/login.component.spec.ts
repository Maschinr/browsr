import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should view modal', () => {
    component.view();
    expect(component.modalOpen).toBe(true);
  });

  it('should login', () => {
    const spy = spyOn(service, 'login').and.returnValue(of({ status: 200 }));
    component.login('username', 'password');
    expect(spy).toHaveBeenCalled();
  });

  it('should cannback', () => {
    const spy = spyOn(service, 'login').and.callThrough();
    component.login('username', 'password');
    expect(spy).toHaveBeenCalled();
  });

  it('should show login success', () => {
    component.onlogin(true);
    expect(component.alert).toBe(true);
  });

  it('should show alert when failed', () => {
    component.onlogin(false);
    expect(component.alert).toBe(false);
  });
});
