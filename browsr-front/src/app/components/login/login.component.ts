import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  modalOpen: boolean = false;
  public alert = true;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  // TODO take route where to navigate to if successfully logged in
  view() {
    this.modalOpen = true;
  }

  login(username: string, password: string) {
    this.userService.login(username, password, (bool: boolean) => { this.onlogin(bool); });
  }

  onlogin(val: boolean) {
    if (val === false) {
      this.alert = false;
    } else {
      this.alert = true;
      this.modalOpen = false;
    }
  }

}
