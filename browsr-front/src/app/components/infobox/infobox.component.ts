import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infobox',
  templateUrl: './infobox.component.html',
  styleUrls: ['./infobox.component.scss']
})
export class InfoboxComponent implements OnInit {

  modalOpen: boolean = false;
  msg: string = '';
  title: string = '';
  constructor() { }

  ngOnInit() {
  }

  view(title: string, msg: string) {
    this.msg = msg;
    this.title = title;
    this.modalOpen = true;
  }

}
