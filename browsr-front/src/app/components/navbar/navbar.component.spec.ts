import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { UserService } from 'src/app/services/user.service';
import { Session } from 'src/app/models/session';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let service: UserService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(UserService);
    router = fixture.debugElement.injector.get(Router);
    fixture.ngZone.run(() => {
      router.initialNavigation();
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout', async(() => {
    fixture.ngZone.run(() => {
      const spy = spyOn(service, 'logout');
      component.logout();
      expect(spy).toHaveBeenCalled();
    });

  }));


  it('should open profile', () => {


    fixture.ngZone.run(async(() => {

      fixture.whenStable().then(() => {
        router.navigate(['']).then(() => {
          console.log('OPEN PROFILE TEST');
          service.session = new Session();
          service.session.username = 'username';
          service.session._uid = 0;

          console.log(service.session);

          component.openProfile();
          console.log('###########################');
        });
      });
    }));
  });

});
