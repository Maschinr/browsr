import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from "@angular/router";
import { InfoboxComponent } from "../infobox/infobox.component";
import { RegisterComponent } from "../register/register.component";
import { LoginComponent } from "../login/login.component";
import { Session } from 'src/app/models/session';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    @ViewChild(RegisterComponent) registerModal: RegisterComponent;
    @ViewChild(LoginComponent) loginModal: LoginComponent;
    burgerOpened = false;

    constructor(public userService: UserService, public router: Router) {

    }

    ngOnInit() {
    }

    logout() {
        if (this.router !== undefined) {
            this.router.navigate(['/highlight']);
        }
        if (this.userService !== undefined) {
            this.userService.logout();
        }

    }

    openProfile() {
        if (this.userService.session !== undefined) {
            this.router.navigate(['/profile/', this.userService.session.username]);
        }
    }

}
