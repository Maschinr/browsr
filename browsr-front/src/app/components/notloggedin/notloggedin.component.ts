import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { RegisterComponent } from "../register/register.component";
import { LoginComponent } from "../login/login.component";

@Component({
  selector: 'app-notloggedin',
  templateUrl: './notloggedin.component.html',
  styleUrls: ['./notloggedin.component.scss']
})
export class NotloggedinComponent implements OnInit {

  private previousRoute: string;
  @ViewChild(RegisterComponent) registerModal: RegisterComponent;
  @ViewChild(LoginComponent) loginModal: LoginComponent;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    if (this.route.params !== undefined) {
      this.route.params.subscribe(params => {
        this.previousRoute = params['key'];
      });
    }

  }

  /*
  login() {
    //login in user service
    //if successfull
    //router.navigate([previousRoute]);
  }
  */

}
