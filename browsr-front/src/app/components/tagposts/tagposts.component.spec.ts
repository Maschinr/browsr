import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagpostsComponent } from './tagposts.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { PostlistComponent } from '../postlist/postlist.component';
import { PostComponent } from '../post/post.component';
import { InfoboxComponent } from '../infobox/infobox.component';
import { AppModule } from 'src/app/app.module';

describe('TagpostsComponent', () => {
  let component: TagpostsComponent;
  let fixture: ComponentFixture<TagpostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagpostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
