import {Component, OnInit, ViewChild} from '@angular/core';
import { ListType } from "../enums/listTypeEnum";
import { ActivatedRoute } from "@angular/router";
import {InfoboxComponent} from "../infobox/infobox.component";
import {PostlistComponent} from "../postlist/postlist.component";

@Component({
  selector: 'app-tagposts',
  templateUrl: './tagposts.component.html',
  styleUrls: ['./tagposts.component.scss']
})
export class TagpostsComponent implements OnInit {

  public listTypeEnum = ListType;
  public tagName: string;

  @ViewChild(PostlistComponent) list: PostlistComponent;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.route.params !== undefined) {
      this.route.params.subscribe(params => {
         this.list.setTagName(params['key']);
      });
    }

  }

}
