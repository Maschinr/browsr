import { Component, OnInit } from '@angular/core';
import {ListType} from "../enums/listTypeEnum";

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss']
})
export class HighlightComponent implements OnInit {

  public listTypeEnum = ListType;

  constructor() { }

  ngOnInit() {
  }

}
