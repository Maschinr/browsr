import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighlightComponent } from './highlight.component';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post';
import { of } from 'rxjs';

describe('HighlightComponent', () => {
  let component: HighlightComponent;
  let fixture: ComponentFixture<HighlightComponent>;
  let service: PostService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlightComponent);
    service = fixture.debugElement.injector.get(PostService);
    const post = new Post();
    post.title = 'test';
    post.id = 0;
    const spy = spyOn(service, 'getAll').and.returnValue(of([post]));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Highlight Component should be created', () => {
    expect(component).toBeTruthy();
  });
});
