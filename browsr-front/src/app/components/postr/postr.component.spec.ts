import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostrComponent } from './postr.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { PostcreationService } from 'src/app/services/postcreation.service';
import { Post } from 'src/app/models/post';
import { Tag } from 'src/app/models/tag';
import { post } from 'selenium-webdriver/http';
import { PostService } from 'src/app/services/post.service';
import { of, throwError } from 'rxjs';
import { TagService } from 'src/app/services/tag.service';

describe('PostrComponent', () => {
  let component: PostrComponent;
  let fixture: ComponentFixture<PostrComponent>;
  let service: PostcreationService;
  let postService: PostService;
  let tagService: TagService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(PostcreationService);
    postService = fixture.debugElement.injector.get(PostService);
    tagService = fixture.debugElement.injector.get(TagService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change primary tag', () => {
    const tag = new Tag();
    tag.name = 'Test 1';
    service.post = new Post();
    component.onTagChange([tag]);

    expect(service.post.ptag).toBe(tag);
  });

  it('should change secondary tag', () => {
    const tagOne = new Tag();
    const tagTwo = new Tag();
    tagOne.name = 'One';
    tagTwo.name = 'Two';
    service.post = new Post();
    component.onTagChange([tagOne, tagTwo]);
    expect(service.post.stag).toBe(tagTwo);
  });

  it('should throw exception when cant create post', () => {
    const spy = spyOn(service, 'post').and.throwError('error');
    component.uploadPost();
  });

  it('should run into error when return is undefined', () => {
    service.post = new Post();
    const spy = spyOn(postService, 'createPost').and.returnValue(undefined);
    component.uploadPost();
    expect(spy).toHaveBeenCalled();
  });

  it('should post successful', () => {
    service.post = new Post();
    const spy = spyOn(postService, 'createPost').and.returnValue(of({ status: 200 }));
    component.uploadPost();
    expect(spy).toHaveBeenCalled();
  });

  it('should throw on invalid input', () => {
    service.post = new Post();
    const spy = spyOn(postService, 'createPost').and.returnValue(throwError({ status: 400 }));
    component.uploadPost();
    expect(spy).toHaveBeenCalled();
  });

  it('should look into the taglist', () => {
    const tag = new Tag();
    const spy = spyOn(tagService, 'getAll').and.returnValue(of([tag]));
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });
});
