import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideopostComponent } from './videopost.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { PostcreationService } from 'src/app/services/postcreation.service';
import { Post } from 'src/app/models/post';
import { post } from 'selenium-webdriver/http';

describe('VideopostComponent', () => {
  let component: VideopostComponent;
  let fixture: ComponentFixture<VideopostComponent>;
  let service: PostcreationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideopostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(PostcreationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be undefined on invalid video URL', () => {
    service.post = new Post();
    service.post.content = '';
    component.urlChanged();
    expect(component.vidFrame).toBe(undefined);
  });

  it('should embed a video from youtube', () => {
    service.post = new Post();
    service.post.content = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';
    component.urlChanged();
    expect(component.vidFrame !== undefined).toBe(true);
  });
});
