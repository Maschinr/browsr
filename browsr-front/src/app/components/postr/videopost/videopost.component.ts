import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PostcreationService} from "../../../services/postcreation.service";
import {PostTypes} from "../../enums/postTypeEnum";
import {EmbedVideoService} from "ngx-embed-video/dist";

@Component({
  selector: 'app-videopost',
  templateUrl: './videopost.component.html',
  styleUrls: ['./videopost.component.scss']
})
export class VideopostComponent implements OnInit {

  vidFrame: any;

  constructor(private router: Router, public postCreationService: PostcreationService, private embedService: EmbedVideoService) {
    this.postCreationService.reset();
    this.postCreationService.post.type = PostTypes.VIDEO;
  }

  ngOnInit() {
  }

  urlChanged() {
    try {
      this.vidFrame = this.embedService.embed(this.postCreationService.post.content);
    } catch (err) {
      this.vidFrame = undefined;
    }
  }

}
