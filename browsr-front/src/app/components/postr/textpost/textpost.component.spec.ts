import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextpostComponent } from './textpost.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';

describe('TextpostComponent', () => {
  let component: TextpostComponent;
  let fixture: ComponentFixture<TextpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });
});
