import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PostcreationService} from "../../../services/postcreation.service";
import {PostTypes} from "../../enums/postTypeEnum";

@Component({
  selector: 'app-textpost',
  templateUrl: './textpost.component.html',
  styleUrls: ['./textpost.component.scss']
})
export class TextpostComponent implements OnInit {

  constructor(public router: Router, public postCreationService: PostcreationService) {
    this.postCreationService.reset();
    this.postCreationService.post.type = PostTypes.TEXT;
  }

  ngOnInit() {
  }

}
