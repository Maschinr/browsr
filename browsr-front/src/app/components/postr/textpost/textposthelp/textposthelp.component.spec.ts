import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextposthelpComponent } from './textposthelp.component';
import { AppModule } from 'src/app/app.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('TextposthelpComponent', () => {
  let component: TextposthelpComponent;
  let fixture: ComponentFixture<TextposthelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextposthelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
