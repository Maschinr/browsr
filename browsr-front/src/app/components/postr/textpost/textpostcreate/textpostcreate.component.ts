import { Component, OnInit } from '@angular/core';
import {PostService} from "../../../../services/post.service";
import {PostcreationService} from "../../../../services/postcreation.service";

@Component({
  selector: 'app-textpostcreate',
  templateUrl: './textpostcreate.component.html',
  styleUrls: ['./textpostcreate.component.scss']
})
export class TextpostcreateComponent implements OnInit {

  constructor(public postCreationService: PostcreationService, private postService: PostService){ }

  ngOnInit() {
  }
}
