import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextpostpreviewComponent } from './textpostpreview.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';

describe('TextpostpreviewComponent', () => {
  let component: TextpostpreviewComponent;
  let fixture: ComponentFixture<TextpostpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextpostpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
