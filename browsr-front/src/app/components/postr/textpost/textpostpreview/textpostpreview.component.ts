import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PostcreationService} from "../../../../services/postcreation.service";

@Component({
  selector: 'app-textpostpreview',
  templateUrl: './textpostpreview.component.html',
  styleUrls: ['./textpostpreview.component.scss']
})
export class TextpostpreviewComponent implements OnInit {

  constructor(public postCreationService: PostcreationService) { }

  ngOnInit() {
  }

}
