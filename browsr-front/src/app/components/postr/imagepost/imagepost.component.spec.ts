import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagepostComponent } from './imagepost.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { PostcreationService } from 'src/app/services/postcreation.service';

describe('ImagepostComponent', () => {
  let component: ImagepostComponent;
  let fixture: ComponentFixture<ImagepostComponent>;
  let service: PostcreationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagepostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(PostcreationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  //HELL NO, NOT TODAY!
  it('should load an image file', () => {
    const mock = new File([''], 'file.png', { type: 'image' });
    const target = { files: [mock] };
    const event = { target: target };
    component.onFileChange(event);
    expect(service.post.content).toBe(mock);
  });


  it('should find invalid file types', () => {
    const mockFile = { name: 'file.exe', size: 24000, type: 'application/binary' };
    const target = { files: [mockFile] };
    const event = { target: target };
    component.onFileChange(event);
    expect(component.info.title).toBe('Error');
  })
});
