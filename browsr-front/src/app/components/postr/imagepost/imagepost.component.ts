import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {PostcreationService} from "../../../services/postcreation.service";
import {PostTypes} from "../../enums/postTypeEnum";
import {InfoboxComponent} from "../../infobox/infobox.component";

@Component({
  selector: 'app-imagepost',
  templateUrl: './imagepost.component.html',
  styleUrls: ['./imagepost.component.scss']
})
export class ImagepostComponent implements OnInit {

  @ViewChild(InfoboxComponent) info: InfoboxComponent;

  imgUrl: any;

  constructor(private router: Router, public postCreationService: PostcreationService) {
    this.postCreationService.reset();
    this.postCreationService.post.type = PostTypes.IMAGE;
  }

  ngOnInit() {
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {

      const type = event.target.files[0].type;
      if (type.match(/image\/*/) == null) {
        this.info.view('Error', 'Invalid file type!');
        return;
      }

      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.imgUrl = reader.result;
      };

      this.postCreationService.post.content = event.target.files[0];
    }
    console.log(this.postCreationService.post.content);
  }
}
