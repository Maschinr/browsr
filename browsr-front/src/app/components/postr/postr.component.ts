import { Component, OnInit, ViewChild } from '@angular/core';
import { PostService } from '../../services/post.service';
import { TagService } from '../../services/tag.service';
import { Tag } from '../../models/tag';
import { PostTypes } from '../enums/postTypeEnum';
import { InfoboxComponent } from "../infobox/infobox.component";
import { Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { PostcreationService } from "../../services/postcreation.service";

@Component({
  selector: 'app-postr',
  templateUrl: './postr.component.html',
  styleUrls: ['./postr.component.scss']
})
export class PostrComponent implements OnInit {

  //TODO warn if switching tabs and content isnt null
  public postType = PostTypes;
  public tagList: Array<Tag> = [];

  public postcreated = false;
  public uploading = false;


  @ViewChild(InfoboxComponent) info: InfoboxComponent;

  constructor(private postCreationService: PostcreationService, private userService: UserService,
    private postService: PostService, private tagService: TagService, private router: Router) {
    if (!userService.isLoggedIn()) {
      router.navigate(['/notLoggedIn/postr']);
    }
  }

  ngOnInit() {
    this.tagService.getAll().subscribe(tags => {
      this.tagList = tags;
    });
  }

  onTagChange(event) {
    if (event !== undefined) {
      if (event[0] !== undefined) {
        this.postCreationService.post.ptag = event[0];
      }
      if (event[1] !== undefined) {
        this.postCreationService.post.stag = event[1];
      }
    }
  }

  uploadPost() {

    let res;
    try {
      res = this.postService.createPost(this.postCreationService.post);
    } catch (err) {
      this.info.view('Error', err);
      return;
    }

    if (res === undefined) {
      this.info.view('Error', 'Unknown Error');
    } else {
      //Go into loading mode
      this.uploading = true;
      res.subscribe(data => {
        console.log('POST SUCCESS');
        this.uploading = false;
        this.postcreated = true;
      }, err => {
        // TODO for different error codes show different errors
        console.log(err);
        this.uploading = false;
        this.info.view('Error', 'Not all required fields are filled!');
      });
    }
  }

}
