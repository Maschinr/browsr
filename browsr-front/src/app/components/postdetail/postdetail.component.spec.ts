import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostdetailComponent } from './postdetail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/services/post.service';
import { of, throwError } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Session } from 'src/app/models/session';
import { ReportService } from 'src/app/services/report.service';

describe('PostdetailComponent', () => {
  let component: PostdetailComponent;
  let fixture: ComponentFixture<PostdetailComponent>;
  let service: PostService;
  let userservice: UserService;
  let reportService: ReportService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostdetailComponent);
    const post = new Post();
    post.id = 0;
    post.title = 'Title';
    service = fixture.debugElement.injector.get(PostService);
    spyOn(service, 'getPostById').and.returnValue(of(post));
    spyOn(service, 'getComments').and.returnValue(of([]));
    userservice = fixture.debugElement.injector.get(UserService);
    reportService = fixture.debugElement.injector.get(ReportService);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a comment', () => {
    const spy = spyOn(service, 'writeComment').and.returnValue(of({ status: 200 }));
    const post = new Post();
    post.id = 0;
    component.post = post;
    component.addComment();
    fixture.detectChanges();
  });

  it('should unauth', () => {
    const spy = spyOn(service, 'writeComment').and.returnValue(throwError({ status: 401 }));
    const post = new Post();
    const user = new Session();
    user._uid = 0;
    user.username = 'Test';
    userservice.session = user;
    post.id = 0;
    component.post = post;
    component.addComment();
    fixture.detectChanges();
  });

  it('should report comment success', () => {
    const spy = spyOn(reportService, 'createReport').and.returnValue(of({ status: 200 }));
    component.report(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should report fail', () => {
    const spy = spyOn(reportService, 'createReport').and.returnValue(throwError({ status: 401 }));
    component.report(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should delete comment success', () => {
    const spy = spyOn(service, 'deleteComment').and.returnValue(of({ status: 200 }));
    component.deleteComment(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should delete comment fail', () => {
    const spy = spyOn(service, 'deleteComment').and.returnValue(throwError({ status: 401 }));
    component.deleteComment(1);
    expect(spy).toHaveBeenCalled();
  });

});
