import {Component, OnInit, ViewChild} from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Post } from 'src/app/models/post';
import { ListType } from "../enums/listTypeEnum";
import { Criteria } from "../enums/criteriaEnum";
import { Direction } from "../enums/directionEnum";
import { UserService } from 'src/app/services/user.service';
import { Comment } from '../../models/comment';
import { Observable } from 'rxjs';
import { CommentStmt } from '@angular/compiler';
import { User } from 'src/app/models/user';
import {ReportService} from "../../services/report.service";
import {InfoboxComponent} from "../infobox/infobox.component";

@Component({
  selector: 'app-postdetail',
  templateUrl: './postdetail.component.html',
  styleUrls: ['./postdetail.component.scss']
})
export class PostdetailComponent implements OnInit {

  public post: Post;
  public comments: Comment[] = [];
  public commenttext = '';
  public modalOpen: boolean = false;
  public parentid: number = undefined;
  @ViewChild(InfoboxComponent) info: InfoboxComponent;

  constructor(private reportService: ReportService, public router: Router, private postservice: PostService, private route: ActivatedRoute, public userservice: UserService) { }

  public addComment() {
    this.postservice.writeComment(this.post.id, this.commenttext, this.parentid).subscribe(data => {
      //Wenn der Kommentar erfolgreich war soll ein Feedback erfolgen. Ums einfach zu halten fügen wir den Kommentar einfach oben an
      this.addCommentToView();
      //TODO: Das geht nicht, denn Jesus hasst uns!
      this.commenttext = '';
    }, error => {
      this.addCommentToView();
      //TODO: Das geht nicht, denn Jesus hasst uns!
      this.commenttext = '';
    });

  }

  deleteComment(id: number) {
    this.postservice.deleteComment(id).subscribe(data => {
      this.info.view("Success", "Comment deleted");
    }, err => {
      this.info.view("Error", "Error deleting Comment");
    });
  }


  addCommentToView() {
    this.postservice.getComments(this.post.id).subscribe(n => {
      this.comments = n;
    });
  }

  ngOnInit() {
    const postId = this.route.snapshot.paramMap.get('id');
    console.log('Details for post: ' + postId);

    this.postservice.getPostById(+postId).subscribe(n => {
      //Das ist fucking retarded, obwohl der Basistyp 1 Objekt ist
      //Bekommen wir an dieser Stelle ein Array
      //Deshalb lassen wir uns nun besagtes Array ausgeben.
      this.post = n[0];
    });

    this.postservice.getComments(+postId).subscribe(n => {
      this.comments = n;
    });
  }

  report(id: number) {
    //TODO show report modal
    this.reportService.createReport("comment", "reason", id).subscribe(data => {

      this.info.view("Info", "Comment reported");
    }, err => {
      this.info.view("Error", "Error reporting comment");
    });
  }


}
