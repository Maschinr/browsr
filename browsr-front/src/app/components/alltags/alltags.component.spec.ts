import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlltagsComponent } from './alltags.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from 'src/app/models/tag';
import { Observable, of } from 'rxjs';

describe('AlltagsComponent', () => {
  let component: AlltagsComponent;
  let fixture: ComponentFixture<AlltagsComponent>;
  let service: TagService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService, TagService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const taglist: Tag[] = [];
    const tagOne = new Tag();
    tagOne.name = 'test';
    tagOne.status = 'ACTIVE';
    tagOne.totalposts = 0;
    taglist.push(tagOne);
    fixture = TestBed.createComponent(AlltagsComponent);
    service = fixture.debugElement.injector.get(TagService);
    const spy = spyOn(service, 'getAll').and.returnValue(of(taglist));
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
    //expect(component.tagList).toEqual(taglist);

  });
});
