import { Component, OnInit } from '@angular/core';
import { Tag } from "../../models/tag";
import { TagService } from "../../services/tag.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-alltags',
  templateUrl: './alltags.component.html',
  styleUrls: ['./alltags.component.scss']
})
export class AlltagsComponent implements OnInit {

  public tagList: Array<Tag> = [];

  constructor(private tagService: TagService, private router: Router) { }

  ngOnInit() {
    const tagsObserver = this.tagService.getAll();
    tagsObserver.subscribe(tags => {
      this.tagList = tags;
    });
  }

}
