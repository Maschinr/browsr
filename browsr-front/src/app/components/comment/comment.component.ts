import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Post } from 'src/app/models/post';
import { TagService } from 'src/app/services/tag.service';
import { PostService } from 'src/app/services/post.service';
import { Ratings } from '../enums/ratingEnum';
import { PostTypes } from '../enums/postTypeEnum';
import { InfoboxComponent } from '../infobox/infobox.component';
import { EmbedVideoService } from "ngx-embed-video/dist";
import { Comment } from '../../models/comment';
import { Tag } from '../../models/tag';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ReportService } from '../../services/report.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {


    @Input() comment: Comment
    postTitle: string = '';
    post: Post;
    posterName: string = '';


    constructor(public reportService: ReportService, public tagService: TagService, public postService: PostService,
        public embedService: EmbedVideoService, public router: Router, public userService: UserService) { }

    ngOnInit() {

        this.postService.getPostById(this.comment.parentpostid)
            .subscribe(data => {
                const o: Post[] = JSON.parse(JSON.stringify(data));
                if (o[0] !== undefined) {
                    this.postTitle = o[0].title;
                }

                // this.posterName = this.comment.poster.username;
            })
    }
}
