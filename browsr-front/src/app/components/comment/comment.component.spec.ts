import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { EmbedVideoService } from 'ngx-embed-video';
import { AppModule } from 'src/app/app.module';
import { Comment as CommentModel } from 'src/app/models/comment';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post';
import { of } from 'rxjs';
import { Tag } from 'src/app/models/tag';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;
  let postservice: PostService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService, EmbedVideoService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    postservice = fixture.debugElement.injector.get(PostService);
    const post = new Post();
    const ptag = new Tag();
    const stag = new Tag();
    ptag.name = 'PTAG';
    stag.name = 'STAG';
    post.id = 0;
    post.content = 'Test';
    post.points = 0;
    post.title = 'Title';
    post.ptag = ptag;
    post.stag = stag;
    post.type = '';
    post.timecreated = new Date(Date.now());
    spyOn(postservice, 'getPostById').and.returnValue(of([post]));
    fixture.componentInstance.comment = new CommentModel();
    fixture.componentInstance.comment.parentpostid = 0;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
