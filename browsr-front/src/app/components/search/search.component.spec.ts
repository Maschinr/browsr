import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { AppModule } from 'src/app/app.module';
import { SearchService } from 'src/app/services/search.service';
import { throwError, of } from 'rxjs';
import { Post } from 'src/app/models/post';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let service: SearchService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      declarations: [],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(SearchService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle search errors', () => {
    const spy = spyOn(service, 'search').and.returnValue(throwError({ status: 401 }));
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  });

  it('should listen to service results', () => {
    const spy = spyOn(service, 'search').and.returnValue(of([new Post()]));
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  });
});
