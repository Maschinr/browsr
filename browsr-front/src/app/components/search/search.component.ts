import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SearchService} from "../../services/search.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  items: Array<any>;

  constructor(private route: ActivatedRoute, private searchService: SearchService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.searchService.search(params['key']).subscribe(data => {
        this.items = data;
      }, err => {
        //TODO Errorbox
        console.error(err);
      });
    });
  }

}
