import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from '@angular/common/http';
import { PostlistComponent } from './components/postlist/postlist.component';
import { HighlightComponent } from './components/highlight/highlight.component';
import { PostComponent } from './components/post/post.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgSelectModule } from '@ng-select/ng-select';
import { PostrComponent } from './components/postr/postr.component';
import { VotrComponent } from './components/votr/votr.component';
import { AccountComponent } from './components/account/account.component';
import { ChangeEmailComponent } from './components/account/change-email/change-email.component';
import { ChangePasswordComponent } from './components/account/change-password/change-password.component';
import { ChangeUsernameComponent } from './components/account/change-username/change-username.component';
import { DeleteAccountComponent } from './components/account/delete-account/delete-account.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';
import { NotloggedinComponent } from './components/notloggedin/notloggedin.component';
import { InfoboxComponent } from './components/infobox/infobox.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { TextpostComponent } from './components/postr/textpost/textpost.component';
import { ImagepostComponent } from './components/postr/imagepost/imagepost.component';
import { VideopostComponent } from './components/postr/videopost/videopost.component';
import { TextpostcreateComponent } from './components/postr/textpost/textpostcreate/textpostcreate.component';
import { TextposthelpComponent } from './components/postr/textpost/textposthelp/textposthelp.component';
import { TextpostpreviewComponent } from './components/postr/textpost/textpostpreview/textpostpreview.component';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
import { EmbedVideo } from 'ngx-embed-video/dist';
import { NgxResponsiveEmbedComponent, NgxResponsiveEmbedModule } from "ngx-responsive-embed";
import { PostdetailComponent } from './components/postdetail/postdetail.component';
import { ModeratorComponent } from './components/moderator/moderator.component';
import { CommentComponent } from './components/comment/comment.component';
import { AlltagsComponent } from "./components/alltags/alltags.component";
import { TagpostsComponent } from './components/tagposts/tagposts.component';
import { UserComponent } from './components/user/user.component';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        PostlistComponent,
        HighlightComponent,
        PostComponent,
        PostrComponent,
        VotrComponent,
        AccountComponent,
        ChangeEmailComponent,
        ChangePasswordComponent,
        ChangeUsernameComponent,
        DeleteAccountComponent,
        AlltagsComponent,
        ProfileComponent,
        SearchComponent,
        NotloggedinComponent,
        InfoboxComponent,
        RegisterComponent,
        LoginComponent,
        TextpostComponent,
        ImagepostComponent,
        VideopostComponent,
        TextpostcreateComponent,
        TextposthelpComponent,
        TextpostpreviewComponent,
        PostdetailComponent,
        ModeratorComponent,
        CommentComponent,
        TagpostsComponent,
        UserComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgSelectModule,
        NgxResponsiveEmbedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MarkdownModule.forRoot({
            markedOptions: {
                provide: MarkedOptions,
                useValue: {
                    gfm: true,
                    tables: true,
                    breaks: true,
                    sanitize: true,
                    smartLists: true,
                    smartypants: true,
                }
            }
        }),
        EmbedVideo.forRoot(),
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [
        CookieService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
