import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HighlightComponent } from './components/highlight/highlight.component';
import { PostrComponent } from './components/postr/postr.component';
import { VotrComponent } from './components/votr/votr.component';
import { AccountComponent } from './components/account/account.component';
import { ChangeEmailComponent } from './components/account/change-email/change-email.component';
import { ChangePasswordComponent } from './components/account/change-password/change-password.component';
import { DeleteAccountComponent } from './components/account/delete-account/delete-account.component';
import { ChangeUsernameComponent } from './components/account/change-username/change-username.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';
import { NotloggedinComponent } from "./components/notloggedin/notloggedin.component";
import { TextpostComponent } from "./components/postr/textpost/textpost.component";
import { ImagepostComponent } from "./components/postr/imagepost/imagepost.component";
import { VideopostComponent } from "./components/postr/videopost/videopost.component";
import { TextpostcreateComponent } from "./components/postr/textpost/textpostcreate/textpostcreate.component";
import { TextposthelpComponent } from "./components/postr/textpost/textposthelp/textposthelp.component";
import { TextpostpreviewComponent } from "./components/postr/textpost/textpostpreview/textpostpreview.component";
import { PostdetailComponent } from "./components/postdetail/postdetail.component";
import { LoginComponent } from './components/login/login.component';
import { ModeratorComponent } from "./components/moderator/moderator.component";
import { AlltagsComponent } from "./components/alltags/alltags.component";
import { TagpostsComponent } from "./components/tagposts/tagposts.component";

const routes: Routes = [
    { path: '', redirectTo: '/highlight', pathMatch: 'full' },
    { path: 'highlight', component: HighlightComponent },
    { path: 'notLoggedIn/:key', component: NotloggedinComponent },
    {
        path: 'profile/:user', component: ProfileComponent
    },
    { path: 'votr', component: VotrComponent },
    { path: 'search/:key', component: SearchComponent },
    { path: 'moderator', component: ModeratorComponent },
    { path: 'alltags', component: AlltagsComponent },
    { path: 'tagposts/:key', component: TagpostsComponent },
    {
        path: 'postr', component: PostrComponent,
        children: [
            { path: '', redirectTo: 'text', pathMatch: 'full' },
            {
                path: 'text', component: TextpostComponent,
                children: [
                    { path: '', redirectTo: 'create', pathMatch: 'full' },
                    { path: 'create', component: TextpostcreateComponent },
                    { path: 'help', component: TextposthelpComponent },
                    { path: 'preview', component: TextpostpreviewComponent }
                ]
            },
            { path: 'image', component: ImagepostComponent },
            { path: 'video', component: VideopostComponent }
        ]
    },
    {
        path: 'account', component: AccountComponent,
        children: [
            { path: '', redirectTo: 'changemail', pathMatch: 'full' },
            { path: 'changemail', component: ChangeEmailComponent },
            { path: 'changepassword', component: ChangePasswordComponent },
            { path: 'changeusername', component: ChangeUsernameComponent },
            { path: 'deleteaccount', component: DeleteAccountComponent }
        ]
    },
    {
        path: 'post/:id', component: PostdetailComponent
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
