import { User } from './user';

export class Comment {
    public _id: number;
    public parentpostid: number;
    public parentid: number;
    public text: string;
    public points: number;
    public userid: number;
    public date: Date;
    public poster: User;

}
