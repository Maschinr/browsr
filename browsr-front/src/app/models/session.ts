export class Session {
  public _uid: number;
  public role: string;
  public username: string;
  public email: string;
  public sessionkey: string;
  public timestamp: string;
}
