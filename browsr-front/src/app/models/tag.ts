/*
Tag:
    properties:
      name:
        type: string
      status:
        type: string
      totalposts:
        type: integer
 */
export class Tag {
  name: string;
  status: string;
  totalposts: number;
}
