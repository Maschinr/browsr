/*
  Post:
    properties:
      id:
        type: number
      poster:
        $ref: "#/definitions/User"
      title:
        type: string
      content:
        type: string
      ptag:
        $ref: "#/definitions/Tag"
      stag:
        $ref: "#/definitions/Tag"
      type:
        type: string
      points:
        type: integer
      status:
        type: string
      timecreate:
        type: date
 */
import {User} from './user';
import {Tag} from './tag';

export class Post {
  id: number;
  poster: User;
  title: string;
  content: any;
  ptag: Tag = new Tag();
  stag: Tag = new Tag();
  type: string;
  points: number;
  status: string;
  timecreated: Date;
}
