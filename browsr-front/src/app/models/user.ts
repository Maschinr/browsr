/*
User:
  properties:
  username:
    type: string
  email:
    type: string
  points:
    type: integer
  role:
    type: string
*/
export class User {
    public username: string;
    public _uid: number;
    public points: number;
    public role: string;
    public email: string;
}
