import {Post} from './post';

export class VoteSession {
  id: number;
  post1: Post;
  post2: Post;
  post3: Post;
  post4: Post;
}
