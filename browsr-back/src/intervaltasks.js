const dbpool = require("./db");
const webpush = require('web-push');

const vapidKeys = {
    publicKey:"BFjBpBFKyBKo5Wsw7LE7Htm4zKT5oGurEFd2ZppwWg-7R5_UpUJkxKjAgjw6PVBNq9oyRgXWpTE5HEswNz4c3gY",
    privateKey:"fsVEuAguQWWFik9AhNFdgUciaTuAZN07SUvxpKo8NWw"
};

webpush.setVapidDetails(
    'mailto:dneuke2@gmail.com',
    vapidKeys.publicKey,
    vapidKeys.privateKey
);

module.exports = async function update() {
    try {

        var dbconnection = await dbpool.getConnection();
        // Do cleaning
        console.log("======================= Start Interval Tasks =======================");

        // Move Posts that were voted successfully to highlighttable
        // Here comes the secret crabby patty formula
        // (activevoting.views >= 100 AND (activevoting.votes /activevoting.view) > 0.7)

        //Send push to highlight people
        dbconnection.query("SELECT posterid FROM posts WHERE _id IN (SELECT postinvoting FROM activevoting WHERE (activevoting.views >= 100 AND (activevoting.votes /activevoting.views) >= 0.7))").then((response) => {
            response.forEach(async user => {
               let notifications = await dbconnection.query("SELECT * FROM users_push WHERE userid = ?", [user]);

                const notificationPayload = {
                    "notification": {
                        "title": "Angular News",
                        "body": "Newsletter Available!",
                        "icon": "assets/main-page-logo-small-hat.png",
                        "vibrate": [100, 50, 100],
                        "data": {
                            "dateOfArrival": Date.now(),
                            "primaryKey": 1
                        },
                        "actions": [{
                            "action": "explore",
                            "title": "Go to the site"
                        }]
                    }
                };

               notifications.forEach(not => {
                   webpush.sendNotification({
                       endpoint: not.endpoint,
                       expirationTime: not.expirationTime,
                       keys: {
                           p256dh: not.p256dh,
                           auth: not.auth
                       }
                   }, JSON.stringify(notificationPayload)) .then(() => {
                       console.log("Sent to")
                   }).catch(err => {
                       console.error("Error sending notification, reason: ", err);
                   });
               });

            });
        });

        //Add to highlightable
        dbconnection.query("INSERT INTO highlights (postid) SELECT postinvoting FROM activevoting WHERE (activevoting.views >= 100 AND (activevoting.votes /activevoting.views) >= 0.7)").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Added " + response.affectedRows + " to highlights\n");
            }
        });

        //Change status of this posts from VOTING to ACTIVE
        dbconnection.query("UPDATE posts SET status=\"ACTIVE\" WHERE _id IN (SELECT postid FROM highlights) AND status='VOTING'").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Changed " + response.affectedRows + " from posts\n");
            }
        });

        // Delete voting_sessions rows where referencecount is zero or which are too old
        dbconnection.query("DELETE FROM voting_sessions WHERE referencecount <= 0 OR timestamp < NOW() - INTERVAL 30 MINUTE").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Deleted " + response.affectedRows + " from voting_sessions\n");
            }
        });

        //Delete users_activevoting binding if post is successfully voted or its too old
        dbconnection.query("DELETE users_activevoting FROM users_activevoting " +
            "INNER JOIN activevoting ON activevoting.postinvoting = users_activevoting.activevotingid " +
            "INNER JOIN posts ON posts._id = users_activevoting.activevotingid "+
            "WHERE (activevoting.views >= 100 AND (activevoting.votes /activevoting.views) >= 0.7) " +
            "OR posts.timecreated < NOW() - INTERVAL 1 DAY").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Deleted " + response.affectedRows + " from users_activevoting\n");
            }
        });

        //Delete posts, activevoting when posts are too old
        dbconnection.query("DELETE posts, activevoting FROM posts " +
            "INNER JOIN activevoting ON posts._id = activevoting.postinvoting "+
            "WHERE posts.timecreated < NOW() - INTERVAL 1 DAY").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Deleted " + response.affectedRows + " from activevoting, posts\n");
            }
        });

        //Delete activevoting if posts are voted
        dbconnection.query("DELETE FROM activevoting "+
            "WHERE (views >= 100 AND (votes / views) >= 0.7)").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Deleted " + response.affectedRows + " from activevoting\n");
            }
        });

        //Change status of this posts from ACTIVE to ARCHIVED
        dbconnection.query("UPDATE posts SET status=\"ARCHIVED\" WHERE _id IN (SELECT postid FROM highlights WHERE timestamp < NOW() - INTERVAL 2 DAY)").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Changed " + response.affectedRows + " from posts\n");
            }
        });

        // Delete the posts from highlight
        dbconnection.query("DELETE FROM highlights WHERE timestamp < NOW() - INTERVAL 2 DAY").then((response) => {
            if(response.warningStatus === 0) {
                console.log("   Deleted " + response.affectedRows + " from highlights\n");
            }
        });
        
    } catch(error) {
        console.log(error);
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
}