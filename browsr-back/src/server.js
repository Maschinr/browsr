const express = require("express");
const app = express();
const fs = require("fs");
const https = require("https");
const bodyParser = require("body-parser");
const userRouter = require("./userrouter");
const contentRouter = require("./contentrouter");
const adminRouter = require("./adminrouter");
const commentRouter = require("./commentrouter");
const achievementRouter = require("./achievementrouter");
const privateKey = fs.readFileSync("./HTTPS/cert.key", "utf8");
const certificate = fs.readFileSync("./HTTPS/cert.pem", "utf8");
const credentials = { key: privateKey, cert: certificate};
const pino = require('pino');
const childProcess = require('child_process');
const stream = require('stream');
const pool = require("./db");
const update = require("./intervaltasks");
const path = require("path");

// Environment variables
const cwd = process.cwd();
const {env} = process;
const logPath = `${cwd}/log`;

// Create a stream where the logs will be written
const logThrough = new stream.PassThrough();
const log = pino({name: 'project'}, logThrough);

// Log to multiple files using a separate process
const child = childProcess.spawn(process.execPath, [
    require.resolve('pino-tee'),
    'fatal', `${logPath}/fatal.log`,
    'error', `${logPath}/error.log`,
    'warn', `${logPath}/warn.log`,
    'info', `${logPath}/info.log`


], {cwd, env});

// INFO = 30; WARN = 40; ERROR = 50; FATAL = 60; <--- level

logThrough.pipe(child.stdin);


/*Middleware*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/scripts", express.static("../node_modules/redoc/bundles/"));
app.use("/", express.static("../../browsr-front/dist/browsr-front/"));
app.use("/swagger.yaml", express.static("./swagger.yaml"));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
/*Routers*/
app.use(userRouter);
app.use(contentRouter);
app.use(commentRouter);
app.use(achievementRouter);
app.use(adminRouter);

app.get("/api", (req, res) => {
    res.sendFile(__dirname + "/docs.html");
});

app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, '../../browsr-front/dist/browsr-front/index.html'), function(err) {
        if (err) {
            res.status(500).send(err)
        }
    });
});

const httpsServer = https.createServer(credentials, app);
httpsServer.listen(3000);
console.log("Server is listening on port 3000!");

var minutes = 1;
var interval = minutes * 60 * 1000;
setInterval(() =>  {
    update();
}, interval);

module.exports = app;