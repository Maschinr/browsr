const express = require("express");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinary_storage = require("multer-storage-cloudinary");

const dbpool = require("./db");
var router = express.Router();

//TODO logs

//Cloudinary setup
cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});

const storage = cloudinary_storage({
    cloudinary: cloudinary,
    folder: "main",
    allowedFormats: ["jpg", "png"]
});

async function getList(listName, tags = undefined, customWhere = "", customQuery = [], start = undefined, limit = undefined, direction = undefined, criteria = undefined) {
    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();
        var sta = 0;
        if(start) {
            if(!isNaN(start)) {
                sta = parseInt(start);
            }
        }

        var lim = 10;
        if(limit) {
            if(!isNaN(limit)) {
                lim = parseInt(limit);
            }
        }

        var dir = "DESC";
        if(direction) {
            if(direction == -1) {
                dir = "ASC";
            }
        }

        var crit = "timecreated";
        if(criteria) {
            if(criteria == "POINTS") {
                crit = "points";
            }
        }

        // Format array for query
        var query = tags;
        var tagString = "";
        var tagQuery = undefined;
        if(tags !== undefined) {
            if(Array.isArray(query)) {
                tagQuery = "(";
                for(var i = 0; i < query.length; i++) {
                    tagQuery += "?";
                    if(i !== query.length - 1) {
                        tagQuery += ", ";
                    }
                }
                tagQuery += ")";
                query = query.concat(query);
            } else if (typeof query === "string") {
                tagQuery = "(?)";
                query = [query, query];
            } else {
                console.error("Invalid query");
                return
            }

            tagString = " WHERE (posts.ptag IN (SELECT _id FROM tags WHERE name IN " + tagQuery + ")\
            OR posts.stag IN (SELECT _id FROM tags WHERE name IN " + tagQuery + "))"

        } else {
            query = [];
        }

        query = query.concat(customQuery);
        query.push(lim);
        query.push(sta);

        console.log(query);

        if(listName === "highlights") {
            listName += " JOIN posts ON highlights.postid = posts._id";
        }

        if(customWhere !== "") {
            if(tags !== undefined) {
                customWhere = " AND " + customWhere;
            } else {
                customWhere = " WHERE " + customWhere;
            }
        }


        const posts = await dbconnection.query("SELECT posts._id AS id, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster,\
        posts.title, posts.content, posts.timecreated,\
        JSON_OBJECT('name', t1.name, 'status', t1.status, 'totalposts', t1.totalposts) AS ptag,\
        JSON_OBJECT('name', t2.name, 'status', t2.status, 'totalposts', t2.totalposts) AS stag,\
        posts.type, posts.points, posts.status\
        FROM " + listName + "\
        JOIN users ON posts.posterid = users._uid\
        INNER JOIN tags AS t1 ON posts.ptag = t1._id\
        INNER JOIN tags AS t2 ON posts.stag = t2._id" + tagString + customWhere + " \
        ORDER BY " + crit + " " + dir + " LIMIT ? OFFSET ?", query);

        posts.forEach(element => {
            element.poster = JSON.parse(element.poster);
            element.ptag = JSON.parse(element.ptag);
            element.stag = JSON.parse(element.stag);
        });

        return posts;
    } catch(error) {
        console.log(error);
        return undefined;
    } finally {
        if(dbconnection) {
            dbconnection.end();
        }
    }
}

//Parser to automatically add the image to the cloud
const parser = multer({storage: storage});

router.post("/api/posts/upload", parser.single("content"), async (req, res) => {
    if(!(req.body.user && req.body.title !== undefined && req.body.title !== "" && req.body.ptag && req.body.stag && req.body.type && req.body.sessionkey && (req.body.content || (req.file != undefined)))) {
        res.status(400).send("Missing Parameters"); 
        return;
    }

    if(!(req.body.type === "TEXT" || req.body.type === "IMAGE" || req.body.type === "VIDEO")) {
        res.status(400).send("Invalid Type"); 
        return;
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        var user = await dbconnection.query("SELECT _uid, username, email, points, role FROM users WHERE username = ? AND sessionkey = ?", [req.body.user, req.body.sessionkey]);

        //Check if User is correct
        if(user.length !== 1) {
            res.status(401).send("Unauthorized User");
            return; 
        }

        //Now check if both the tags are valid
        var tags = await dbconnection.query("SELECT * FROM tags WHERE name = ? OR name = ? AND status = 'Active'", [req.body.ptag, req.body.stag]);
        
        if(tags.length !== 2) {
            res.status(400).send("Invalid Tags");
            return;
        }

        var postData = {
            posterid: user[0]._uid,
            title: req.body.title,
            content: "",
            ptag: tags[0]._id,
            stag: tags[1]._id,
            type: req.body.type,
            points: 0,
            status: 'VOTING'
        }

        if(req.file !== undefined) { //It's a picture uploaded to cloudinary
            //postData.contentid = req.file.public_id;
            postData.content = req.file.secure_url;
        } else if(req.body.content) { //Its a link to an external site
            postData.content = req.body.content;
        }

        let postid = await dbconnection.query("SELECT _id FROM posts WHERE posterid=? AND title=? AND content=? AND ptag=? AND stag=? AND type=? AND points=? AND status=?",
                                                [postData.posterid, postData.title, postData.content, postData.ptag, postData.stag, postData.type, postData.points, postData.status]);

        if(postid.length !== 0) {
            res.status(400).send("Duplicate Post");
            return; 
        }
        
        let response = await dbconnection.query("INSERT INTO posts (posterid, title, content, ptag, stag, type, points, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                                                [postData.posterid, postData.title, postData.content, postData.ptag, postData.stag, postData.type, postData.points, postData.status]);
        if(response.warningStatus !== 0) {
            res.status(500).send();
            return; 
        }

        postid = await dbconnection.query("SELECT _id FROM posts WHERE posterid=? AND title=? AND content=? AND ptag=? AND stag=? AND type=? AND points=? AND status=?",
                                                [postData.posterid, postData.title, postData.content, postData.ptag, postData.stag, postData.type, postData.points, postData.status]);


        response = await dbconnection.query("UPDATE tags SET totalposts = (totalposts + 1) WHERE name = ? OR name = ?", [tags[0].name, tags[1].name]);

        if(response.warningStatus !== 0) {
            res.status(500).send();
            return;
        }

        response = await dbconnection.query("INSERT INTO activevoting (postinvoting) VALUES (?)", [postid[0]._id]);
        
        if(response.warningStatus !== 0) {
            res.status(500).send();
            return; 
        } else {
            res.status(200).send({
                id: postid[0]._id,
                poster: {
                    username: user[0].username,
                    email: user[0].email, 
                    points: user[0].points, 
                    role: user[0].role
                },
                title: postData.title,
                content: postData.content,
                ptag: {
                    name: tags[0].name,
                    status: tags[0].status,
                    totalposts: tags[0].totalposts
                },
                stag: {
                    name: tags[1].name,
                    status: tags[1].status,
                    totalposts: tags[1].totalposts
                },
                type: postData.type,
                points: postData.points,
                status: postData.status
            });
        }
    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/posts/highlight", async (req, res) => {

    const posts = await getList("highlights", req.query.tags, "", [], req.query.start, req.query.limit, req.query.direction, req.query.criteria);

    if(posts !== undefined) {
        res.status(200).send(posts);
    } else {
        res.status(500).send("Internal Server Error");
        console.error("Error in highlight");
    }
});

router.get("/api/posts/archive", async (req, res) => {

    const posts = await getList("posts", req.query.tags,  "posts.status='ARCHIVED'", [], req.query.start, req.query.limit, req.query.direction, req.query.criteria);

    if(posts !== undefined) {
        res.status(200).send(posts);
    } else {
        res.status(500).send("Internal Server Error");
        console.error("Error in archive");
    }

});

router.get("/api/posts/user", async (req, res) => {
    if(!req.query.user) {
        res.status(400).send("Missing Parameters");
        return;
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        var user = await dbconnection.query("SELECT * FROM users WHERE username=?", [req.query.user]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
        }

        const posts = await getList("posts", req.query.tags,  "posts.posterid=? AND (posts.status='ARCHIVED' OR posts.status='ACTIVE')", [user[0]._uid], req.query.start, req.query.limit, req.query.direction, req.query.criteria);

        if(posts !== undefined) {
            res.status(200).send(posts);
        } else {
            res.status(500).send("Internal Server Error");
            console.error("Error in user");
        }

        res.status(200).send(posts);

    } catch(error) {
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/vote", async (req, res) => {
    //Create voting session
    if(!(req.query.type && req.query.tags && req.query.user && req.query.sessionkey)) {
        res.status(400).send("Missing Parameters");
        return;
    }
    if(!(req.query.type === "TEXT" || req.query.type === "IMAGE" || req.query.type === "VIDEO")) {
        res.status(400).send("Invalid type");
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();
        
        var user = await dbconnection.query("SELECT * FROM users WHERE username = ? AND sessionkey = ?", [req.query.user, req.query.sessionkey]);

        //Check if User is correct
        if(user.length !== 1) {
            res.status(401).send("Unauthorized User");
            return; 
        }

        var query = req.query.tags;
        var tagQuery = undefined;
        if(query !== undefined) {
            if(typeof query !== "string") {
                tagQuery = "(";

                for(var i = 0; i < query.length; i++) {
                    tagQuery += "?";
                    if(i != query.length - 1) {
                        tagQuery += ", ";
                    }
                }
    
                tagQuery += ")";
                query = query.concat(query);
            } else {
                tagQuery = "(?)";
                query = [query, query];
            }
        }

        query.unshift(user[0]._uid);
        query.unshift(req.query.type);

        //Fetch 4 posts with given criterias
        var posts = await dbconnection.query("SELECT * FROM activevoting\
        JOIN posts ON activevoting.postinvoting = posts._id\
        WHERE posts.type = ?\
        AND activevoting.postinvoting NOT IN (SELECT activevotingid FROM users_activevoting WHERE userseenid = ?)\
        AND NOT (activevoting.views >= 100 AND (activevoting.votes / activevoting.views) >= 0.7)\
        AND posts.timecreated > NOW() - INTERVAL 23 HOUR\
        AND (ptag IN (SELECT _id FROM tags WHERE name IN " + tagQuery + ")\
        OR stag IN (SELECT _id FROM tags WHERE name IN " + tagQuery + "))\
        ORDER BY RAND() LIMIT 4", query);

        if(posts.length !== 4) {
            res.status(400).send("Not enough posts with given criteria to create vote session");
            return;
        }

        var voteSession = await dbconnection.query("SELECT * FROM voting_sessions WHERE post1id = ? AND post2id = ? AND post3id = ? AND post4id = ?", [posts[0]._id, posts[1]._id, posts[2]._id, posts[3]._id]);
        
        if(voteSession.length === 1) { // Vote session already exists, use it
            var response = await dbconnection.query("UPDATE voting_sessions SET timestamp = CURRENT_TIMESTAMP, referencecount = (referencecount + 1) WHERE _id = ?", [voteSession[0]._id]);

            if(response.warningStatus !== 0) {
                res.status(500).send("Failed to update referencecount");
                return;
            }
        } else { // Create new vote session
            voteSession = await dbconnection.query("INSERT INTO voting_sessions (post1id, post2id, post3id, post4id) VALUES (?, ?, ?, ?)", [posts[0]._id, posts[1]._id, posts[2]._id, posts[3]._id]);

            if(voteSession.warningStatus !== 0) {
                res.status(500).send("Failed to create vote session");
                return;
            }
        }
        var voteSession = await dbconnection.query("SELECT * FROM voting_sessions WHERE post1id = ? AND post2id = ? AND post3id = ? AND post4id = ?", [posts[0]._id, posts[1]._id, posts[2]._id, posts[3]._id]);
        if(voteSession.length !== 1) {
            res.status(500).send("Failed to get vote session");
            return;
        }
        var posts = await dbconnection.query("SELECT posts._id AS id, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster,\
            posts.title, posts.content, posts.timecreated,\
            JSON_OBJECT('name', t1.name, 'status', t1.status, 'totalposts', t1.totalposts) AS ptag,\
            JSON_OBJECT('name', t2.name, 'status', t2.status, 'totalposts', t2.totalposts) AS stag,\
            posts.type, posts.points, posts.status, posts._id\
            FROM posts\
            JOIN users ON posts.posterid = users._uid\
            INNER JOIN tags AS t1 ON posts.ptag = t1._id\
            INNER JOIN tags AS t2 ON posts.stag = t2._id\
            WHERE posts._id IN (?, ?, ?, ?)", [posts[0]._id, posts[1]._id, posts[2]._id, posts[3]._id]);

        //Reformat the array
        var postResponse = {
            id: voteSession[0]._id,
            post1: undefined,
            post2: undefined,
            post3: undefined,
            post4: undefined
        };

        //TODO replace?
        for(var i = 0; i < posts.length; i++) {
            posts[i].poster = JSON.parse(posts[i].poster);
            posts[i].ptag = JSON.parse(posts[i].ptag);
            posts[i].stag = JSON.parse(posts[i].stag);
            
            if(posts[i]._id == voteSession[0].post1id) {
                postResponse.post1 = posts[i];
            } else if(posts[i]._id == voteSession[0].post2id) {
                postResponse.post2 = posts[i];
            } else if(posts[i]._id == voteSession[0].post3id) {
                postResponse.post3 = posts[i];
            } else {
                postResponse.post4 = posts[i];
            }
        }

        res.status(200).send(postResponse);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/vote", async (req, res) => {
    //Vote for one of the posts in an votingsession
    if(!(req.body.voteSessionId && req.body.voted && req.body.user && req.body.sessionkey)) {
        res.status(400).send("Missing Paramters");
        return;
    }

    if(req.body.voted < 1 || req.body.voted > 4) {
        res.status(400).send("Voted too high/low");
        return;
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        var user = await dbconnection.query("SELECT * FROM users WHERE username = ? AND sessionkey = ?", [req.body.user, req.body.sessionkey]);
            
        //Check if User is correct
        if(user.length !== 1) {
            res.status(401).send("Unauthorized User");
            return; 
        }

        var votingSession = await dbconnection.query("SELECT * FROM voting_sessions WHERE _id = ?", [req.body.voteSessionId]);

        if(votingSession[0] === undefined) {
            res.status(400).send("Invalid sessionid");
            return;
        }

        var votedPostId;
        if(req.body.voted == 1) {
            votedPostId = votingSession[0].post1id;
        } else if(req.body.voted == 2) {
            votedPostId = votingSession[0].post2id;
        } else if(req.body.voted == 3) {
            votedPostId = votingSession[0].post3id;
        } else {
            votedPostId = votingSession[0].post4id;
        }
        let response = await dbconnection.query("UPDATE activevoting SET views = (views + 1) WHERE postinvoting IN (?, ?, ? ,?)",
            [votingSession[0].post1id, votingSession[0].post2id, votingSession[0].post3id, votingSession[0].post4id]);

        if(response.warningStatus !== 0) {
            res.status(500).send("failed to increment view count");
            return;
        }

        response = await dbconnection.query("UPDATE activevoting SET votes = (votes + 1) WHERE postinvoting = ?", [votedPostId]);

        if(response.warningStatus !== 0) {
            res.status(500).send("failed to increment voted count");
            return;
        }

        response = await dbconnection.query("UPDATE voting_sessions SET referencecount = (referencecount - 1) WHERE _id = ?", [req.body.voteSessionId]);

        if(response.warningStatus !== 0) {
            res.status(500).send("failed to decrement reference count");
            return;
        }

        response = await dbconnection.query("DELETE FROM voting_sessions WHERE referencecount <= 0");
        if(response.warningStatus !== 0) {
            res.status(500).send("Failed to delete voting session");
            return;
        }

        //TODO check if post is now fully voted for performance?
        //var post = await dbconnection.query("SELECT * FROM activevoting JOIN posts ON activevoting.postinvoting = posts._id WHERE postinvoting = ?", [req.body.voted]);

        //if(post.length !== 1) {
        //    res.status(500).send("Failed to fetch voted post");
        //   return;
        //}


        //Add all posts of votingsession to users_activevoting for this user
        response = await dbconnection.query("INSERT INTO users_activevoting (activevotingid, userseenid) VALUES (?, ?), (?, ?) ,(?, ?), (?, ?)", [votingSession[0].post1id, user[0]._uid, votingSession[0].post2id, user[0]._uid, votingSession[0].post3id, user[0]._uid, votingSession[0].post4id, user[0]._uid]);
        if(response.warningStatus !== 0) {
            res.status(500).send("Failed to insert user voted binding");
            return;
        }
        res.status(200).send();
    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/posts/rate", async (req, res) => {
    if(!(req.body.user && req.body.sessionkey && req.body.postid && req.body.rating && (req.body.rating === "DOWN" || req.body.rating === "UP"))) {
        res.status(400).send("Missing Parameters");
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        //Check if users is valid
        var user = await dbconnection.query("SELECT * FROM users WHERE username = ? AND sessionkey = ?", [req.body.user, req.body.sessionkey]);   
        //Check if User is correct
        if(user.length !== 1) {
            res.status(401).send("Unauthorized User");
            return; 
        }
        user = user[0];

        //Check if posts exists
        var post = await dbconnection.query("SELECT * FROM posts WHERE _id =?", [req.body.postid]);
        if(post.length !== 1) {
            res.status(400).send("Non existing post");
            return; 
        }

        if(post[0].status == "VOTING") {
            res.status(400).send("Cant rate posts that are in voting");
            return;
        }

        //Check if he has already rated it
        var valuechange = 0;
        var response = await dbconnection.query("SELECT * FROM users_ratedposts WHERE usid=? AND poid=?", [user._uid, req.body.postid]);
        if(response.length === 1) { // he has rated this post
            if(response[0].vote === "UP" && req.body.rating === "DOWN") {
                valuechange = -2;
            } else if(response[0].vote === "DOWN" && req.body.rating === "UP") {
                valuechange = 2;
            }

            if(valuechange !== 0) {
                //Change users_ratedposts voting
                response = await dbconnection.query("UPDATE users_ratedposts SET vote=? WHERE usid =? AND poid=?", [req.body.rating, user._uid, req.body.postid]);
                if(response.warningStatus !== 0) {
                    res.status(500).send("Failed to create user post binding");
                    return;
                }
            }
        } else { // he has not rated this post
            if(req.body.rating === "DOWN") {
                valuechange = -1;
            } else if(req.body.rating === "UP") {
                valuechange = 1;
            }
            //Insert into users_posts the postid and the user too
            response = await dbconnection.query("INSERT INTO users_ratedposts (usid, poid, vote) VALUES (?, ?, ?)", [user._uid, req.body.postid, req.body.rating]);
            if(response.warningStatus !== 0) {
                res.status(500).send("Failed to create user post binding");
                return;
            }
        }

        if(valuechange === 0) { // Nothing has changed don't to sql request
            res.status(400).send("Already rated this post with this value");
            return;
        }

        //Increment/Decrement points of post
        response = await dbconnection.query("UPDATE posts SET points = (points + ?) WHERE posts._id=?", [valuechange, req.body.postid]);
        if(response.warningStatus !== 0) {
            res.status(500).send("Failed to change points value in post");
            return;
        }

        let points = {
            points: post[0].points + valuechange
        };

        res.status(200).send(points);
        

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/user/addPush", async (req,res) => {
    console.log(req.body.sub);
    if (!req.body.user || !req.body.sessionkey || !req.body.sub || !req.body.sub.endpoint || !req.body.sub.keys.p256dh || !req.body.sub.keys.auth) {
        res.status(400).send("Missing Parameters");
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();


        //Check if users is valid
        var user = await dbconnection.query("SELECT * FROM users WHERE username = ? AND sessionkey = ?", [req.body.user, req.body.sessionkey]);
        //Check if User is correct
        if(user.length !== 1) {
            res.status(401).send("Unauthorized User");
            return;
        }
        user = user[0];

        //TODO push subscription object
        let response = await dbconnection.query("SELECT * FROM users_push WHERE endpoint =?", [req.body.sub.endpoint]);

        if(response.length !== 0) {
            res.status(200).send();
            return;
        }

        response = await dbconnection.query("INSERT INTO users_push (userid, endpoint, expirationTime, p256dh, auth) VALUES (?, ?, ?, ?, ?)", [user._uid, req.body.sub.endpoint, req.body.sub.expirationTime, req.body.sub.keys.p256dh, req.body.sub.keys.auth]);

        res.status(200).send();

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/posts/single", async (req, res) => {
    if (!req.query.postid) {
        res.status(400).send("Missing Parameters")
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        var response = await dbconnection.query("SELECT posts._id AS id, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster, " +
            "posts.title, posts.content, posts.timecreated," +
            "JSON_OBJECT('name', t1.name, 'status', t1.status, 'totalposts', t1.totalposts) AS ptag, " +
            "JSON_OBJECT('name', t2.name, 'status', t2.status, 'totalposts', t2.totalposts) AS stag, " +
            "posts.type, posts.points, posts.status " +
            "FROM posts " +
            "JOIN users ON posts.posterid = users._uid " +
            "INNER JOIN tags AS t1 ON posts.ptag = t1._id " +
            "INNER JOIN tags AS t2 ON posts.stag = t2._id "  +
            "WHERE posts._id = ?", [req.query.postid]);

        response.forEach(element => {
            element.poster = JSON.parse(element.poster);
            element.ptag = JSON.parse(element.ptag);
            element.stag = JSON.parse(element.stag);
        });

        res.status(200).send(response);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/tags", async (req, res) => {
    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        var response = await dbconnection.query("SELECT name, status, totalposts FROM tags");
        res.status(200).send(response);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/posts/tag", async(req, res) => {
    if (!req.query.name) {
        res.status(400).send("Missing Parameters");
        return
    }
    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let response = await dbconnection.query("SELECT name, totalposts FROM tags WHERE name = ?", [req.query.name]);

        if (response.length !== 1) {
            res.status(400).send("Invalid Name");
            return
        }

        const posts = await getList("posts", req.query.name,  "(posts.status='ARCHIVED' OR posts.status='ACTIVE')", [], req.query.start, req.query.limit, req.query.direction, req.query.criteria);

        res.status(200).send(posts);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/search", async (req, res) => {
    if(!req.query.search) {
        res.status(400).send("Missing Parameters");
        return;
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();
        req.query.search = "%" + req.query.search + "%";
        var users = await dbconnection.query("SELECT username, email, points, role FROM users WHERE username LIKE ?", [req.query.search]);

        var posts = await dbconnection.query("SELECT posts._id AS id, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster,\
        posts.title, posts.content, posts.timecreated,\
        JSON_OBJECT('name', t1.name, 'status', t1.status, 'totalposts', t1.totalposts) AS ptag,\
        JSON_OBJECT('name', t2.name, 'status', t2.status, 'totalposts', t2.totalposts) AS stag,\
        posts.type, posts.points, posts.status FROM posts\
        JOIN users ON posts.posterid = users._uid\
        INNER JOIN tags AS t1 ON posts.ptag = t1._id\
        INNER JOIN tags AS t2 ON posts.stag = t2._id\
        WHERE (posts.title LIKE ? OR t1.name LIKE ? OR t2.name LIKE ? or posts.content LIKE ?) AND (posts.status = 'ACTIVE' OR posts.status = 'ARCHIVED')", [req.query.search, req.query.search, req.query.search, req.query.search]);
  
        var tags = await dbconnection.query("SELECT name, status, totalposts FROM tags WHERE name LIKE ?", [req.query.search]);
        
        //Append all to users array and send it  on his way
        users.forEach(element => {
            element.itemType = "user";
        });

        posts.forEach(element => {
            element.itemType = "post";
            element.poster = JSON.parse( element.poster);
            element.ptag = JSON.parse(element.ptag);
            element.stag = JSON.parse(element.stag);
            users.push(element);
        });

        tags.forEach(element => {
            element.itemType = "tag";
            users.push(element);
        });

        res.status(200).send(users); 
        

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("")

module.exports = router;