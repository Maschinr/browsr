const mariadb = require('mariadb');
const pool = mariadb.createPool({
    host: 'localhost',
    user: 'daniel',
    password: '',
    connectionLimit: 100,
    database: 'browsr'
});

module.exports = pool;