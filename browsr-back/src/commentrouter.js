const express = require("express");
const pool = require("./db");

var router = express.Router();


//Functions

async function postComment(req, res) {
    if(req.body.sessionkey && req.body.postid && req.body.text) {
        var conn;
        var parentid;
        
        try {
            conn = await pool.getConnection();
            if(req.body.parentid === undefined) {
                parentid = null;
            } else {
                parentid = req.body.parentid;
                var parentcomment = await conn.query("SELECT * FROM comments WHERE _id=?", [parentid]);
                if(parentcomment.length !== 1) {
                    res.status(400).send("Parent comment not found! ");
                    return;
                }
            }
            var user = await conn.query("SELECT * FROM users WHERE sessionkey=?", [req.body.sessionkey]);
            if(user.length !== 1) {
                res.status(401).send("Unauthorized");
                return;
            }
            var post = await conn.query("SELECT * FROM posts WHERE _id=?",[req.body.postid]);
            if(post.length !== 1) {
                res.status(400).send("Post not found");
                return;
            }
            var response = await conn.query("INSERT INTO comments (parentpostid, parentid, text, userid) VALUES (?,?,?,?)",[req.body.postid, parentid, req.body.text, user[0]._uid]);
            if(response.warningStatus !== 0) {
                res.status(400).send("Couldnt push into db");
            }
            res.status(200).send("Comment saved");
        } catch(err) {
            res.status(400).send(err);

        } finally {
            if(conn) return conn.end();
        }
    }
}

async function getComments(req, res) {
    if(req.query.postid) {
        var conn;
        var start;
        var limit;
        var sort;
        try {
            conn = await pool.getConnection();
            if(req.body.start) {
                if(!isNaN(req.query.start)) {
                    start = parseInt(req.query.start);
                } else {
                    res.status(400).send("start is NaN");
                    return;
                }
            } else {
                start = 0;
            }

            if(req.query.limit) {
                if(!isNaN(req.query.limit)) {
                    limit = parseInt(req.query.limit);
                } else {
                    res.status(400).send("limit is NaN");
                    return;
                }
            } else {
                limit = 20;
            }
            if(req.query.criteria) {
                sort = "comments." + req.query.criteria;
            } else {
                sort = "comments.date";
            }
            var comments = {};
            if(!req.query.parentid) {
                comments = await conn.query("SELECT comments.*, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster FROM comments JOIN users ON comments.userid = users._uid WHERE comments.parentpostid=? ORDER BY " + sort + " ASC LIMIT ? OFFSET ?", [req.query.postid, limit, start]);
            } else {
                comments = await conn.query("SELECT comments.*, JSON_OBJECT('username', users.username, 'email', users.email, 'points', users.points, 'role', users.role) AS poster FROM comments JOIN users ON comments.userid = users._uid WHERE comments.parentpostid=? AND comments.parentid=? ORDER BY " + sort + " ASC LIMIT ? OFFSET ?", [req.query.postid, req.query.parentid, limit, start]);
            }

            comments.forEach(element => {
                element.poster = JSON.parse(element.poster);
            });

            res.status(200).send(comments);

        } catch(err) {

            res.status(400).send(err);

        } finally {
            if(conn) return conn.end();
        }

    } else {
        res.status(400).send("Missing Parameters")
    }
}

async function getCommentsFromUser(req, res) {
    if(req.query.userid) {
        var conn;
        var sort = "date";
        if(req.query.sort && parseInt(req.query.sort) === 1) {
            sort = "points";
        }
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT * FROM comments WHERE userid = ? ORDER BY " + sort + " ASC", [req.query.userid]);

            res.status(200).send(response);
        } catch(err) {
            res.status(400).send(err);
        } finally {
            if(conn) return conn.end();
        }
    } else {
        res.status(400).send("userid not specified");
    }
}

async function deleteCommentsById(req, res) {
    if(req.body.id && req.body.sessionkey) {
        var conn;
        try {
            conn = await pool.getConnection();
            var user = await conn.query("SELECT * FROM users WHERE sessionkey = ? AND (role = ? OR role = ? OR role = ?)", [req.body.sessionkey, "MODERATOR", "ADMIN", "DEVELOPER"]);
            var commentToDelete = await conn.query("SELECT * FROM comments WHERE _id = ? AND userid = (SELECT _uid FROM users WHERE sessionkey = ?)", [req.body.id, req.body.sessionkey]);
            if(user.length === 1 || commentToDelete.length > 0) {
                var resp = await conn.query("DELETE FROM reports WHERE reportedcommentid=?", [req.body.id]);
                var response = await conn.query("DELETE FROM comments WHERE _id = ? OR parentpostid = ?", [req.body.id, req.body.id]);
                res.status(200).send();
            } else {
                res.status(401).send("user not authorized to delete comment");
            }
        } catch(err) {
            res.status(400).send(err);
        } finally {
            if(conn) return conn.end();
        }
    } else {
        res.status(400).send("Not all needed values specified")
    }
}



router.post("/api/comment/postcomment", postComment);
router.get("/api/comment/getcomments", getComments);
router.get("/api/comment/getcommentsfromuser", getCommentsFromUser);
router.post("/api/comment/deletecomment", deleteCommentsById)


module.exports = router;