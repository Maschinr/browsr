const express = require("express");
const pool = require("./db");

var router = express.Router();


async function getAllAchievements(req, res) {
    var func = async function() {
        if(true) {
        console.log("success");
        }
    };
    func = func.toString();
    func = func.substring(func.indexOf("{") + 1, func.lastIndexOf("}"));
    eval(func);
    res.status(200).send(func);
}

async function getUserAchievements(req, res) {
    if(req.query.id) {
    }

}

async function checkUserAchievements(req, res) {
    if(req.body.id) {
    
    }
}

async function gefaelltMir(req) {
    var conn;
    var achieved = false;
    try {
        conn = await pool.getConnection();
        var result = await conn.query("SELECT COUNT(usid) AS rated FROM users_ratedposts WHERE usid = ? AND vote = 'UP'", [req.body.id]);
        if(parseInt(result[0].rated) > 0) {
            achieved = true;
        }
    } catch(err) {

    } finally {
        if(conn) conn.close();
    }
}

async function gefaelltMirSehr(req) {
    var conn;
    var achieved = false;
    try {
        conn = await pool.getConnection();
        var result = await conn.query("SELECT COUNT(usid) AS rated FROM users_ratedposts WHERE usid = ? AND vote = 'UP'", [req.body.id]);
        if(parseInt(result[0].rated) >= 50) {
            achieved = true;
        }
    } catch(err) {

    } finally {
        if(conn) conn.close();
    }
}

async function likeGott(req) {
    var conn;
    var achieved = false;
    try {
        conn = await pool.getConnection();
        var result = await conn.query("SELECT COUNT(usid) AS rated FROM users_ratedposts WHERE usid = ? AND vote = 'UP'", [req.body.id]);
        if(parseInt(result[0].rated) >= 100) {
            achieved = true;
        }
    } catch(err) {

    } finally {
        if(conn) conn.close();
    }
}

router.get("/api/achievements/getallachievements", getAllAchievements);
router.get("/api/achievements/getuserachievements", getUserAchievements);
router.post("/api/achievements/checkuserachievements", checkUserAchievements);


module.exports = router;