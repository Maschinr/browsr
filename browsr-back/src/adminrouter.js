const express = require("express");
const dbpool = require("./db");
var router = express.Router();

router.post("/api/tags/remove", async (req, res) => {
    if (!req.body.name || !req.body.user || !req.body.sessionkey) {
        res.status(400).send("Missing Parameters");
        return
    }
    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=? AND role='MODERATOR'", [req.body.user, req.body.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
        }

        let response = await dbconnection.query("SELECT name, totalposts FROM tags WHERE name = ?", [req.body.name]);

        if (response.length !== 1) {
            res.status(400).send("Invalid Name");
            return
        }

        if (response[0].totalposts !== 0) {
            res.status(400).send("Tag is used by posts")
            return
        }

        response = await dbconnection.query("DELETE FROM tags WHERE name = ?", [req.body.name]);

        res.status(200).send();

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/tags/create", async (req, res) => {
    if (!req.body.name || !req.body.user || !req.body.sessionkey) {
        res.status(400).send("Missing Parameters");
        return
    }
    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=? AND role='MODERATOR'", [req.body.user, req.body.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
        }

        let response = await dbconnection.query("SELECT name, totalposts FROM tags WHERE name = ?", [req.body.name]);

        if (response.length !== 0) {
            res.status(400).send("Invalid Name");
            return
        }
        response = await dbconnection.query("INSERT INTO tags (name) VALUES (?)", [req.body.name]);

        res.status(200).send();

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.get("/api/reports", async (req,res) => {
    if (!req.query.user || !req.query.sessionkey) {
        res.status(400).send("Missing Parameters");
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=? AND role='MODERATOR'", [req.query.user, req.query.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
        }

        const posts = await dbconnection.query("SELECT reports.reason, reports.status, reports._id AS reportid, reppost.content AS content, reppost.title AS posttitle, reppost._id AS postid, reppost._id AS reportedid, reporter.username AS repname  FROM reports " +
            "INNER JOIN posts AS reppost ON reports.reportedpostid = reppost._id " +
            "INNER JOIN users AS reporter ON reports.reporterid = reporter._uid");


        posts.forEach(element => {
            element.type = "post";
        });

        const response = posts.concat(await dbconnection.query("SELECT reports.reason, reports.status, reports._id AS reportid, repcom.text as content, repcom._id AS reportedid, repcom.parentpostid AS postid, reporter.username AS repname FROM reports " +
            "INNER JOIN comments AS repcom ON reports.reportedcommentid = repcom._id " +
            "INNER JOIN users AS reporter ON reports.reporterid = reporter._uid"));


        response.forEach(element => {
            if(!element.type) {
                element.type = "comment";
            }
        });

        res.status(200).send(response);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/reports/create", async (req,res) => {
    if (!req.body.user || !req.body.sessionkey || !req.body.type || !req.body.id || !req.body.reason) {
        res.status(400).send("Missing Parameters");
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=?", [req.body.user, req.body.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
            return;
        }

        let response;
        if(req.body.type === "post") {

            let post = await dbconnection.query("SELECT * FROM posts WHERE _id=?", [req.body.id]);

            if(post.length !== 1) {
                res.status(400).send("Invalid post");
                return;
            }

            response = await dbconnection.query("INSERT INTO reports (reportedpostid, reason, reporterid, status) VALUES (?, ?, ?, 'PENDING')", [post[0]._id, req.body.reason, user[0]._uid]);

        } else if (req.body.type === "comment") {

            let comment = await dbconnection.query("SELECT * FROM comments WHERE _id=?", [req.body.id]);

            if(comment.length !== 1) {
                res.status(400).send("Invalid comment");
                return;
            }


            response = await dbconnection.query("INSERT INTO reports (reportedcommentid, reason, reporterid, status) VALUES (?, ?, ?, 'PENDING')", [comment[0]._id, req.body.reason, user[0]._uid]);

        } else {
            res.status(400).send("Invalid type");
            return;
        }


        res.status(200).send(response);

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/reports/delete", async (req,res) => {
    if (!req.body.user || !req.body.sessionkey || !req.body.id) {
        res.status(400).send("Missing Parameters");
        return;
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=? AND role='MODERATOR'", [req.body.user, req.body.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
            return;
        }

        let response = await dbconnection.query("DELETE FROM reports WHERE _id = ?", [req.body.id]);

        if (response.warningStatus !== 0) {
            res.status(500).send();
            return;
        }

        res.status(200).send();

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

router.post("/api/posts/delete", async (req, res) => {
    if (!req.body.postid || !req.body.user || !req.body.sessionkey) {
        res.status(400).send("Missing Parameters")
        return
    }

    var dbconnection;
    try {
        dbconnection = await dbpool.getConnection();

        let user = await dbconnection.query("SELECT * FROM users WHERE username=? AND sessionkey=? AND role='MODERATOR'", [req.body.user, req.body.sessionkey]);
        if(user.length !== 1) {
            res.status(400).send("Invalid User");
            return;
        }

        let response = await dbconnection.query("SELECT * FROM posts WHERE _id = ?", [req.body.postid]);

        if (response.length !== 1) {
            res.status(400).send("Invalid Post");
            return
        }

        //Delete comments and reports for comments
        response = await dbconnection.query("DELETE comments, reports FROM comments " +
            "INNER JOIN reports ON comments._id = reports.reportedcommentid WHERE parentpostid = ?", [req.body.postid]);

        if(response.warningStatus !== 0) {
            res.status(500).send()
            return;
        }

        //Delete reports for this post
        response = await dbconnection.query("DELETE FROM reports WHERE reportedpostid = ?", [req.body.postid]);

        if(response.warningStatus !== 0) {
            res.status(500).send()
            return;
        }

        //Delete ratings
        response = await dbconnection.query("DELETE FROM users_ratedposts WHERE poid = ?", [req.body.postid]);

        if(response.warningStatus !== 0) {
            res.status(500).send()
            return;
        }

        //Delete highlights
        response = await dbconnection.query("DELETE FROM highlights WHERE postid = ?", [req.body.postid]);

        if(response.warningStatus !== 0) {
            res.status(500).send()
            return;
        }

        //Delete posts
        response = await dbconnection.query("DELETE FROM posts WHERE _id = ?", [req.body.postid]);

        if(response.warningStatus !== 0) {
            res.status(500).send()
            return;
        }

        res.status(200).send();

    } catch(error) {
        console.log(error);
        res.status(500).send("Connection to db failed");
        return;
    } finally {
        if(dbconnection) {
            return dbconnection.end();
        }
    }
});

module.exports = router;