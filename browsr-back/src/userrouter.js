const express = require("express");
const pool = require("./db");
const uuidv4 = require("uuid/v4");

var router = express.Router();



/*Functions*/

async function register(req, res) {
    if (req.body.username && req.body.email && req.body.password) {
        var conn;
        var salt;
        var hashedPassword;
        try {
            var crypto = require("crypto")
        } catch (err) {
            res.status(400).send("{error : true, errortype : crypto error}");
            return;
        }
        salt = crypto.randomBytes(64);
        crypto.pbkdf2(req.body.password, salt.toString("base64"), 100000, 256, "sha512", async function (err, result) {
            if (err) {
                res.status(400).send("{error : true, errortype : error while hashing password");
                return;
            }
            hashedPassword = result;

            try {
                conn = await pool.getConnection();
                const dbres = await conn.query("INSERT INTO users (username, email, password, salt, role) VALUES (?,?,?,?,?)", [req.body.username, req.body.email, hashedPassword.toString("base64"), salt.toString("base64"), "USER"])
                res.status(200).send("User added");

            } catch (err) {
                res.status(400).send(err);
            } finally {
                if (conn) return conn.end();
            }
        });

    } else {
        res.status(400).send("{error : true, errortype : required fields not filled!}");
    }
}
async function login(req, res) {
    if ((req.body.username || req.body.email) && req.body.password) {
        var conn;
        var salt;
        var hashedPassword;
        var storedPassword;
        try {
            var crypto = require("crypto")
        } catch (err) {
            res.status(400).send("{error : true, errortype : crypto error}");
            return;
        }
        if (req.body.email) {
            findParams = { email: req.body.email }
        }
        try {
            conn = await pool.getConnection();
            var dbresult = await conn.query("SELECT * FROM users WHERE username=?;", [req.body.username]);
            salt = dbresult[0].salt;
            storedPassword = dbresult[0].password;
            crypto.pbkdf2(req.body.password, salt, 100000, 256, "sha512", async function (err, result) {
                if (err) {
                    res.status(400).send("{error : true, errortype : error while hashing password");
                    return;
                }
                hashedPassword = result.toString("base64");
                if (storedPassword === hashedPassword) {
                    if (dbresult[0].sessionkey) {
                        var response = await conn.query("UPDATE users SET timestamp=CURRENT_TIMESTAMP WHERE username=?;", [req.body.username]);
                        result = await conn.query("SELECT _uid, username, sessionkey, timestamp, points, role FROM users WHERE username=?;", [req.body.username]);
                        res.status(200).send(result);

                    } else {
                        var response = await conn.query("UPDATE users SET sessionkey=?, timestamp=CURRENT_TIMESTAMP WHERE username=?;", [uuidv4(), req.body.username]);
                        result = await conn.query("SELECT _uid, username, sessionkey, timestamp, points, role FROM users WHERE username=?;", [req.body.username]);
                        res.status(200).send(result);
                    }
                } else {
                    res.status(400).send("{error : true, errortype : wrong password}");
                }
            });

        } catch (err) {
            res.status(400).send(err);

        } finally {
            if (conn) return conn.end();
        }

    } else if (req.body.id && req.body.sessionkey) {
        var conn;
        try {
            conn = await pool.getConnection();
            var result = await conn.query("SELECT * FROM users WHERE _uid = ? AND sessionkey = ?", [req.body.id, req.body.sessionkey]);
            if (result.length === 1) {
                var r = await conn.query("UPDATE users SET timestamp=CURRENT_TIMESTAMP WHERE _uid = ?", [req.body.id]);
                result = await conn.query("SELECT _uid, username, sessionkey, timestamp, points, role FROM users WHERE _uid = ? AND sessionkey = ?", [req.body.id, req.body.sessionkey]);
                res.status(200).send(result);
            } else {
                res.status(401).send("Unauthorized");
            }
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }
    } else {
        res.status(400).send("{error: true, errortype : required fields not filled!}")
    }
}
async function changeUsername(req, res) {
    if (req.body.sessionkey && req.body.password && req.body.newname) {
        var conn;
        try {
            conn = await pool.getConnection();
            var user = await conn.query("SELECT * FROM users WHERE sessionkey=?", [req.body.sessionkey]);
            if (user.length !== 1) {
                if (conn) conn.end();
                res.status(401).send("Unauthorized");
                return;
            }
            try {
                var crypto = require("crypto")
            } catch (err) {
                res.status(400).send("{error : true, errortype : crypto error}");
                return;
            }
            var salt;
            var hashedPassword;
            var storedPassword;
            salt = user[0].salt;
            storedPassword = user[0].password;
            crypto.pbkdf2(req.body.password, salt, 100000, 256, "sha512", async function (err, result) {
                if (err) {
                    res.status(400).send("{error : true, errortype : error while hashing password");
                    return;
                }
                hashedPassword = result.toString("base64");
                if (storedPassword === hashedPassword) {
                    var response = await conn.query("UPDATE users SET username=? WHERE sessionkey=?", [req.body.newname, req.body.sessionkey]);
                    res.status(200).send("Username changed");
                } else {
                    res.status(401).send("Unauthorized");
                }
            });

        } catch (err) {
            res.status(400).send(err);

        } finally {
            if (conn) return conn.end();
        }

    } else {
        res.status(401).send("Unauthorized");
    }
}
async function changePassword(req, res) {
    if (req.body.sessionkey && req.body.oldpassword && req.body.newpassword) {
        var conn;

        try {
            conn = await pool.getConnection();
            var user = await conn.query("SELECT * FROM users WHERE sessionkey=?;", [req.body.sessionkey]);
            if (user.length !== 1) {
                if (conn) conn.end();
                res.status(401).send("Unauthorized");
                return;
            }
            try {
                var crypto = require("crypto")
            } catch (err) {
                res.status(400).send("{error : true, errortype : crypto error}");
                return;
            }
            var salt;
            var hashedPassword;
            var storedPassword;
            salt = user[0].salt;
            storedPassword = user[0].password;
            crypto.pbkdf2(req.body.oldpassword, salt, 100000, 256, "sha512", async function (err, result) {
                if (err) {
                    res.status(400).send("{error : true, errortype : error while hashing password");
                    return;
                }
                hashedPassword = result.toString("base64");
                if (storedPassword === hashedPassword) {
                    salt = crypto.randomBytes(64).toString("base64")
                    crypto.pbkdf2(req.body.newpassword, salt, 100000, 256, "sha512", async function (err, result) {
                        if (err) {
                            res.status(400).send("{error : true, errortype : error while hashing password}");
                            return
                        }
                        var response = await conn.query("UPDATE users SET salt=?, password=? WHERE sessionkey=?", [salt, result.toString("base64"), req.body.sessionkey]);
                        if (response.warningStatus === 0) res.status(200).send("Password changed");
                        else res.status(400).send("Something went terribly wrong")
                    });
                } else {
                    res.status(401).send("Unauthorized");
                }
            });
        } catch (err) {
            res.status(400).send(err);

        } finally {
            if (conn) return conn.end();
        }

    } else {
        res.status(401).send("Unauthorized");
    }

}
async function changeEmail(req, res) {
    if (req.body.sessionkey && req.body.password && req.body.oldemail && req.body.newemail) {
        var conn;
        try {
            conn = await pool.getConnection();
            var user = await conn.query("SELECT * FROM users WHERE sessionkey=? AND email=?", [req.body.sessionkey, req.body.oldemail]);
            if (user.length !== 1) {
                res.status(401).send("Unauthorized");
                return;
            }
            try {
                var crypto = require("crypto")
            } catch (err) {
                res.status(400).send("{error : true, errortype : crypto error}");
                return;
            }
            var salt;
            var hashedPassword;
            var storedPassword;
            salt = user[0].salt;
            storedPassword = user[0].password;
            crypto.pbkdf2(req.body.password, salt, 100000, 256, "sha512", async function (err, result) {
                if (err) {
                    res.status(400).send("{error : true, errortype : error while hashing password");
                    return;
                }
                hashedPassword = result.toString("base64");
                if (storedPassword === hashedPassword) {
                    var response = await conn.query("UPDATE users SET email=? WHERE sessionkey=?", [req.body.newemail, req.body.sessionkey]);
                    if (response.warningStatus !== 0) {
                        res.status(400).send("Bad request");
                        return;
                    }
                    res.status(200).send("Email changed");
                } else {
                    res.status(401).send("Unauthorized");
                }
            });
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }


    } else {
        res.status(401).send("Unauthorized");
    }
}
//TODO: Delete account problematisch wegen foreign keys in der datenbank... User dürfen nicht gelöscht werden, da dependencies zu anderen tables exisiteren..
//Evtl Lösung: Bei delete Username auf "deleteduser + UUID" setzen, flag im User auf inactive setzen und password auf ein zufälliges passwort setzen, damit 
//Eine account recovery durch den user selbst unmöglich wird. Mit Datenbankaccess lässt sich durch einen Admin der account dennoch recovern.
async function deleteAccount(req, res) {
    if (req.body.sessionkey && req.body.email && req.body.password) {
        var conn;
        var searchString = {
            sessionkey: req.body.sessionkey,
            email: req.body.email
        };
        try {
            conn = await pool.getConnection();
            var user = await conn.query("SELECT * FROM users WHERE sessionkey=? AND email=?", [req.body.sessionkey, req.body.email]);
            if (user.length !== 1) {
                res.status(401).send("Unauthorized");
            }
            try {
                var crypto = require("crypto")
            } catch (err) {
                res.status(400).send("{error : true, errortype : crypto error}");
                return;
            }
            var salt;
            var hashedPassword;
            var storedPassword;
            salt = user[0].salt;
            storedPassword = user[0].password;
            crypto.pbkdf2(req.body.password, salt, 100000, 256, "sha512", async function (err, result) {
                if (err) {
                    res.status(400).send("{error : true, errortype : error while hashing password");
                    return;
                }
                hashedPassword = result.toString("base64");
                if (storedPassword === hashedPassword) {
                    var response = await conn.query("DELETE FROM users WHERE sessionkey=?", [req.body.sessionkey]);
                    if (response.warningStatus !== 0) {
                        res.status(400).send("Something went terribly wrong");
                        return;
                    }
                    res.status(200).send("User deleted");
                } else {
                    res.status(401).send("Unauthorized");
                }
            });
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }

    } else {
        res.status(401).send("Unauthorized");
    }
}

async function getUser(req, res) {
    if (req.query.id) {
        var conn;
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT _uid, username, points, role FROM users WHERE _uid = ?", [req.query.id]);
            res.status(200).send(response);
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }
    } else if (req.query.sessionkey) {
        var conn;
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT _uid, username, points, role FROM users WHERE sessionkey = ?", [req.query.sessionkey]);
            res.status(200).send(response);
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }
    } else if (req.query.exactUsername) {
        var conn;
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT _uid, username, points, role FROM users WHERE username = ?", [req.query.exactUsername]);
            res.status(200).send(response);
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }
    } else if (req.query.usernamePattern) {
        var conn;
        var pattern = '"%' + req.query.usernamePattern + '%"';
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT _uid, username, points, role FROM users WHERE username LIKE " + pattern);
            res.status(200).send(response);
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }

    } else if (req.query.email) {
        var conn;
        try {
            conn = await pool.getConnection();
            var response = await conn.query("SELECT _uid, username, points, role FROM users WHERE email = ?", [req.query.email]);
            res.status(200).send(response);
        } catch (err) {
            res.status(400).send(err);
        } finally {
            if (conn) return conn.end();
        }

    } else {
        res.status(400).send("no field specified");
    }
}

router.post("/api/account/register", register);
router.post("/api/account/login", login);
router.post("/api/account/changeusername", changeUsername);
router.post("/api/account/changepassword", changePassword);
router.post("/api/account/changeemail", changeEmail);
router.post("/api/account/deleteaccount", deleteAccount);
router.get("/api/account/getuser", getUser);



module.exports = router;